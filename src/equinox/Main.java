package equinox;

import equinox.expanders.*;
import equinox.explicit.NbtSerializers;
import equinox.explicit.*;
import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import equinox.remap.*;
import equinox.util.GenericAwareType;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Main {
    public static final int NAME_LENGTH_TO_BE_UNOBFUSCATED = 4;


    public static void main(String[] args) throws IOException {
        File output = new File("new");
        File input = new File("old");
        File outputSrgExplicit = new File(output, "joined.srg");
        if (outputSrgExplicit.exists())
            outputSrgExplicit.delete();

        Fingerprint old = new Fingerprint(input, FileBackedRemap.Read(input, "joined"));
        Fingerprint nw = new Fingerprint(output, new CompositeRemap(new CustomRemap(), new LinkedRemap(old.Remap)));

        old.LoadAll();
        nw.LoadAll();

//        if (1>0)
//            return;

//        Class d = nw.GetClass("d");
//        for (Method m : d.Methods())
//            System.out.println(m);
//        if (1 > 0)
//            return;

        PerformExpand(old, nw, new IExpander[]{
                new BiomesNamer(),
                new BlocksNamer(),
                new EnchantmentsNamer(),
                new EntitiesNamer(),
                new ItemsNamer(),
                new MobEffectNamer(),
                new PotionsNamer(),
                new SoundEventNamer(),
                new PresetStringPoolMethodNamer(),
                new TextureRefClassNamer(),
                new StructureNamer(),
                new ToStringNamer(),
                new CommandsNamer(),
                new StatsNamer(),
                new UniqueStringConstantClassNamer(),
                new StatePropertiesNamer()
        });
        PerformExpand(old, nw, new IExpander[]{
                new SuperExpander(),
                new NbtSerializers(),
                new InterfaceMapper(),
                new MethodSignatureExpander(),
                new SingleMethodMapper(),
                new GetSetToFieldExpander(),
                new FieldToGetSetExpander(),
                new FieldFieldExpander(),
                new NestedForTypeExpander(),
                new RendererToModelExpander(),
                new EnumNameExpander(),
                new ConstantReturnMethodExpander(),
                new MethodProxyMethodExpander(),
                new UnobfuscatedMethodExpander(),
                new NestedToExpander(),
                new UniqueStringConstantMethodExpander(),
                new FieldTypeExpander()
        });

        {
            CompositeRemap cr = (CompositeRemap) nw.Remap;
            CustomRemap custom = Arrays.stream(cr.Databases).filter(x -> x instanceof CustomRemap).map(x -> (CustomRemap) x).findFirst().get();
            LinkedRemap linked = Arrays.stream(cr.Databases).filter(x -> x instanceof LinkedRemap).map(x -> (LinkedRemap) x).findFirst().get();


//            Class br = nw.GetClass("pc$a");
//            for (Method m : br.Methods()) {
//                System.out.println(m + "\tLinked=" + linked.Linked(m));
//            }
//            System.exit(0);

            Set<Method> explored = new HashSet<>();
            List<Set<Method>> groups = new ArrayList<>();
            for (Class c : nw.Classes()) {
                if (!c.isRemapped() && c.IsMinecraft && !c.Backing.getClassName().contains("/")) {
                    StringBuilder name = new StringBuilder();
                    Class root = c;
                    while (root != null) {
                        if (name.length() > 0)
                            name.insert(0, '$');
                        if (root.isRemapped())
                            name.insert(0, root.getNestingAwareRemappedName());
                        else
                            name.insert(0, "class_miss_" + root.getNestingAwareObfuscatedName() + "_" + Integer.toHexString(root.getObfuscatedName().hashCode()));
                        root = root.NestedFor;
                    }
                    String pkg = "net/minecraft/unmapped/";
                    if (c.InheritsFrom("com/mojang/datafixers/DataFix") || c.InheritsFrom("com/mojang/datafixers/schemas/Schema"))
                        pkg = "net/minecraft/datafix/";
                    else if (c.InheritsFrom("net/minecraft/network/Packet"))
                        pkg = "net/minecraft/network/packets/";
                    custom.Remap(c, pkg + name.toString());
                }
                if (c.IsMinecraft) {
                    for (Field f : c.Fields())
                        if (!f.isRemapped() && f.getObfuscatedName().length() < NAME_LENGTH_TO_BE_UNOBFUSCATED)
                            custom.Remap(f, "field_miss_" + f.Backing.getName() + "_" + Integer.toHexString(f.hashCode()));

                    for (Method m : c.Methods())
                        if (!m.isStaticCtor() && !m.isCtor() && !m.isRemapped() && !explored.contains(m) && !m.Group().stream().anyMatch(x -> !x.Parent.IsMinecraft)) {
                            Set<Method> group = m.Group();
                            groups.add(group);
                            for (Method k : group)
                                if (!explored.add(k))
                                    System.out.println("Double listed method :/");
                        }
                }
            }

            // force consistency for mapped classes:
            boolean changing = true;
            while (changing) {
                changing = false;
                for (Class c : nw.Classes()) {
                    if (c.NestedFor == null)
                        continue;
                    String remapped = c.getRemappedName();
                    String pureName = remapped.substring(remapped.lastIndexOf('$') + 1);
                    String wantedName = c.NestedFor.getRemappedName() + "$" + pureName;
                    if (!remapped.equals(wantedName)) {
                        custom.Remap(c, wantedName);
                        changing = true;
                    }
                }
            }

            for (Set<Method> group : groups) {
                if (group.stream().anyMatch(x -> x.IsUnobfuscated || x.Parent.IsUnobfuscated || x.isRemapped()))
                    continue;
                String baseName = group.iterator().next().Backing.getName();
                if (baseName.length() >= NAME_LENGTH_TO_BE_UNOBFUSCATED)
                    continue;
                String newName = "method_miss_" + baseName + "_" + Integer.toHexString(group.hashCode());
                for (Method m : group) {
                    custom.Remap(m, newName);
                }
            }
        }

        FileBackedRemap.Create((IEnumerableRemapDatabase) nw.Remap).Save(outputSrgExplicit, "srg");
        new FileBackedRemap().Read(outputSrgExplicit);
    }

    private static void PerformExpand(Fingerprint old, Fingerprint nw, IExpander[] expanders) {
        CompositeRemap cr = (CompositeRemap) nw.Remap;
        CustomRemap custom = Arrays.stream(cr.Databases).filter(x -> x instanceof CustomRemap).map(x -> (CustomRemap) x).findFirst().get();
        LinkedRemap linked = Arrays.stream(cr.Databases).filter(x -> x instanceof LinkedRemap).map(x -> (LinkedRemap) x).findFirst().get();
        while (true) {
            boolean modOuter = false;
            for (IExpander expander : expanders) {
                expander.Init(old, nw, custom, linked);
                while (true) {
                    int delta = expander.Expand();
                    if (delta == 0)
                        break;
//                    System.out.println(expander.getClass().getSimpleName() + " changed " + delta);
                    modOuter |= true;
                    if (!expander.RepeatUntilStable())
                        break;
                }
            }
            if (!modOuter)
                return;
        }
    }
}

package equinox.explicit;

import equinox.expanders.DualExplicitMapper;
import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CommandsNamer extends DualExplicitMapper {
    private static final String _prefix = "commands.";

    @Override
    protected Map<String, Object> Compute(Fingerprint fp) {
        Set<String> badKeys = new HashSet<>();
        Map<String, Object> res = new HashMap<>();
        outer:
        for (Class c : fp.Classes()) {
            String key = null;
            for (String s : c.StringConstants()) {
                if (s.startsWith(_prefix)) {
                    int nextDot = s.indexOf('.', _prefix.length() + 1);
                    String tmpKey = s.substring(_prefix.length(), nextDot > 0 ? nextDot : s.length());
                    if (key != null && !key.equals(tmpKey))
                        continue outer;
                    key = tmpKey;
                }
            }
            if (key == null)
                continue outer;
            key = "Command" + Character.toUpperCase(key.charAt(0)) + key.substring(1).toLowerCase();
            if (badKeys.contains(key))
                continue outer;
            if (res.remove(key) != null)
                badKeys.add(key);
            else
                res.put(key, c);
        }
        return res;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return false;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

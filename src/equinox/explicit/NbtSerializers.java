package equinox.explicit;

import equinox.expanders.DualExplicitMapper;
import equinox.fingerprint.Field;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.generic.*;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

public class NbtSerializers extends DualExplicitMapper {
    private Method FindGroup(Fingerprint fp, String find) {
        return PresetStringPoolMethodNamer.FindGroup(fp, find);
    }

    @Override
    protected Map<String, Object> Compute(Fingerprint fp) {
        Map<String, Object> result = new HashMap<>();
        Method saveToNbt = FindGroup(fp, "Saving entity NBT");
        if (saveToNbt != null) {
            result.put("SaveEntityToNBT", saveToNbt);
            MapNbtMethod(fp, saveToNbt, result);
        }
        Method loadFromNbt = FindGroup(fp, "Loading entity NBT");
        if (loadFromNbt != null) result.put("LoadEntityFromNBT", loadFromNbt);
        return result;
    }

    private void MapNbtMethod(Fingerprint fp, Method saveToNbt, Map<String, Object> mapping) {
        if (saveToNbt.Backing.getArgumentTypes().length != 1) {
            System.out.println("SaveEntityToNBT format changed, many fields won't get named " + saveToNbt);
            return;
        }
        String nbtCompoundType = null;
        {
            Type nbtCompoundTypeHandle = saveToNbt.Backing.getArgumentTypes()[0];
            if (nbtCompoundTypeHandle instanceof ObjectType)
                nbtCompoundType = ((ObjectType) nbtCompoundTypeHandle).getClassName();
        }
        if (nbtCompoundType == null) {
            System.out.println("SaveEntityToNBT format changed, many fields won't get named " + saveToNbt);
            return;
        }
        Set<Method> methodsExplored = new HashSet<>();
        Stack<Method> methodsForExplore = new Stack<>();
        methodsForExplore.push(saveToNbt);
        Map<String, Set<Object>> usedTags = new HashMap<>();

        while (!methodsForExplore.empty()) {
            Method user = methodsForExplore.pop();
            if (!methodsExplored.add(user))
                continue;
            for (Method child : user.Group())
                methodsForExplore.push(child);
            Code code = user.Backing.getCode();
            if (code == null)
                continue;
            InstructionList il = new InstructionList(code.getCode());
            ConstantPoolGen cpg = new ConstantPoolGen(code.getConstantPool());
            Instruction[] set = il.getInstructions();
            for (int id = 3; id < il.size(); id++) {
                Instruction i = set[id];
                if (i instanceof INVOKEVIRTUAL) {
                    INVOKEVIRTUAL call = (INVOKEVIRTUAL) i;
                    String typeName = call.getClassName(cpg);
                    String signature = call.getSignature(cpg);
                    for (int mode = 0; mode <= 1; mode++) {
                        // void method(String, something) is likely a setter
                        if (typeName.equals(nbtCompoundType) && signature.startsWith("(Ljava/lang/String;") && signature.endsWith("V")) {
                            String nbtKeyName = null;
                            Object storageHandle = null;
                            if (mode == 0) {
                                Instruction[] history = new Instruction[3];
                                for (int hi = 0, ti = 1; hi < history.length && id >= ti; ti++) {
                                    if (set[id - ti] instanceof ConversionInstruction)
                                        continue;
                                    history[hi++] = set[id - ti];
                                }
                                Instruction keyName = history[2];
                                if (!(keyName instanceof LDC) || ((LDC) keyName).getType(cpg) != Type.STRING)
                                    continue;
                                String nbtKeyRaw = ((LDC) keyName).getValue(cpg).toString();
                                nbtKeyName = "entityNbt" + Character.toUpperCase(nbtKeyRaw.charAt(0)) + nbtKeyRaw.substring(1);
                                if (user.Parent.isRemapped())
                                    nbtKeyName = user.Parent.getRemappedName() + "#" + nbtKeyName;
                                Instruction selfLoad = history[1];
                                if (!(selfLoad instanceof ALOAD) || ((ALOAD) selfLoad).getIndex() != 0)
                                    continue;
                                Instruction getFieldOrInvoke = history[0];
                                if (getFieldOrInvoke instanceof GETFIELD) {
                                    String fieldName = ((GETFIELD) getFieldOrInvoke).getFieldName(cpg);
                                    Type fieldDeclaredOn = ((GETFIELD) getFieldOrInvoke).getReferenceType(cpg);
                                    if (!(fieldDeclaredOn instanceof ObjectType) || !((ObjectType) fieldDeclaredOn).getClassName().equals(user.Parent.Backing.getClassName()))
                                        continue;
                                    String fieldDeclaring = ((ObjectType) fieldDeclaredOn).getClassName();
                                    if (!fp.IsMinecraft(fieldDeclaring))
                                        continue;
                                    Field fieldHandle = fp.GetClass(fieldDeclaring).GetField(fieldName);
                                    if (fieldHandle == null)
                                        continue;
                                    storageHandle = fieldHandle;
                                } else if (getFieldOrInvoke instanceof INVOKEVIRTUAL) {
                                    String methodName = ((INVOKEVIRTUAL) getFieldOrInvoke).getMethodName(cpg);
                                    Type methodDeclaredOn = ((INVOKEVIRTUAL) getFieldOrInvoke).getReferenceType(cpg);
                                    if (!(methodDeclaredOn instanceof ObjectType) || !((ObjectType) methodDeclaredOn).getClassName().equals(user.Parent.Backing.getClassName()))
                                        continue;
                                    String fieldDeclaring = ((ObjectType) methodDeclaredOn).getClassName();
                                    if (!fp.IsMinecraft(fieldDeclaring))
                                        continue;
                                    Method methodHandle = fp.GetClass(fieldDeclaring).GetMethod(methodName, ((INVOKEVIRTUAL) getFieldOrInvoke).getSignature(cpg));
                                    if (methodHandle == null || methodHandle.Backing.getArgumentTypes().length > 0)
                                        continue;
                                    nbtKeyName = "get" + Character.toUpperCase(nbtKeyName.charAt(0)) + nbtKeyName.substring(1);
                                    if (methodHandle.Parent.isRemapped())
                                        nbtKeyName = methodHandle.Parent.getRemappedName() + "#" + nbtKeyName;
                                    storageHandle = methodHandle;
                                }
                            } else if (set[id - 1] instanceof InvokeInstruction) {
                                Type[] argTypes = ((InvokeInstruction) set[id - 1]).getArgumentTypes(cpg);
                                if (argTypes.length == 0 || !(argTypes[argTypes.length - 1] instanceof ArrayType))
                                    continue;
                                int j = id - 2;
                                List<Object> handles = new ArrayList<>();
                                boolean arrayPrimed = false;
                                while (j >= 0) {
                                    Instruction test = set[j];
                                    j--;
                                    if (test instanceof GETFIELD && arrayPrimed) {
                                        arrayPrimed = false;
                                        String fieldName = ((GETFIELD) test).getFieldName(cpg);
                                        Type fieldDeclaredOn = ((GETFIELD) test).getReferenceType(cpg);
                                        if (!(fieldDeclaredOn instanceof ObjectType) || !((ObjectType) fieldDeclaredOn).getClassName().equals(user.Parent.Backing.getClassName()))
                                            continue;
                                        String fieldDeclaring = ((ObjectType) fieldDeclaredOn).getClassName();
                                        if (!fp.IsMinecraft(fieldDeclaring))
                                            continue;
                                        Field fieldHandle = fp.GetClass(fieldDeclaring).GetField(fieldName);
                                        if (fieldHandle == null)
                                            continue;
                                        handles.add(fieldHandle);
                                    }
                                    if (test instanceof ArrayInstruction && test instanceof StackConsumer)
                                        arrayPrimed = true;
                                    if (test instanceof LDC) {
                                        if (((LDC) test).getType(cpg) != Type.STRING)
                                            break;
                                        String nbtKeyRaw = ((LDC) test).getValue(cpg).toString();
                                        nbtKeyName = "nbt" + Character.toUpperCase(nbtKeyRaw.charAt(0)) + nbtKeyRaw.substring(1);
                                        if (user.Parent.isRemapped())
                                            nbtKeyName = user.Parent.getRemappedName() + "#" + nbtKeyName;
                                        else
                                            nbtKeyName = "unknown_" + nbtKeyName;
                                        break;
                                    }
                                }
                                if (nbtKeyName != null)
                                    storageHandle = handles.toArray();
                            }

                            if (storageHandle == null)
                                continue;
                            if (storageHandle instanceof Object[]) {
                                Object[] array = (Object[]) storageHandle;
                                for (int aij = 0; aij < array.length; aij++) {
                                    String subKey = nbtKeyName + "_" + aij;
                                    Set<Object> usedBy = usedTags.get(subKey);
                                    if (usedBy != null) {
                                        usedBy.add(array[aij]);
                                        mapping.remove(subKey);
                                    } else {
                                        usedBy = new HashSet<>();
                                        usedBy.add(array[aij]);
                                        usedTags.put(subKey, usedBy);
                                        mapping.put(subKey, array[aij]);
                                    }
                                }
                            } else {
                                Set<Object> usedBy = usedTags.get(nbtKeyName);
                                if (usedBy != null) {
                                    usedBy.add(storageHandle);
                                    mapping.remove(nbtKeyName);
                                } else {
                                    usedBy = new HashSet<>();
                                    usedBy.add(storageHandle);
                                    usedTags.put(nbtKeyName, usedBy);
                                    mapping.put(nbtKeyName, storageHandle);
                                }
                            }
                        }
                    }
                    // bounces the NBT compound to another call.  We need to explore this call.
                    if (typeName.equals(user.Parent.Backing.getClassName()) && signature.equals("(L" + nbtCompoundType + ";)V")) {
                        Instruction loadSelf = set[id - 2];
                        Instruction loadNBT = set[id - 1];
                        if (!(loadSelf instanceof ALOAD) || ((ALOAD) loadSelf).getIndex() != 0)
                            continue;
                        if (!(loadNBT instanceof ALOAD) || ((ALOAD) loadNBT).getIndex() != 1)
                            continue;
                        Method bounce = user.Parent.GetMethod(call.getMethodName(cpg), signature);
                        if (bounce != null)
                            methodsForExplore.push(bounce);
                    }
                }
            }
        }

        usedTags.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).forEach(k -> {
            if (k.getValue().size() > 1)
                System.out.println("NBT Tag " + k.getKey() + " resolved to multiples, will not be mapped " + k.getValue());
//            System.out.println(k.getKey() + " -> " + k.getValue());
        });
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return true;
    }
}

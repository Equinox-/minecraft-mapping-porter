package equinox.explicit;

public class EnchantmentsNamer extends DatabaseNamer {
    public EnchantmentsNamer() {
        super(
                "protection",
                "fire_protection",
                "feather_falling",
                "blast_protection",
                "projectile_protection",
                "respiration",
                "aqua_affinity",
                "thorns",
                "depth_strider",
                "frost_walker",
                "binding_curse",
                "sharpness",
                "smite",
                "bane_of_arthropods",
                "knockback",
                "fire_aspect",
                "looting",
                "sweeping",
                "efficiency",
                "silk_touch",
                "unbreaking",
                "fortune",
                "power",
                "punch",
                "flame",
                "infinity",
                "luck_of_the_sea",
                "lure",
                "mending",
                "vanishing_curse");
    }

    @Override
    protected String getTypeName(String n) {
        return "net/minecraft/enchantment/Enchantment" + tagToName(n);
    }

    @Override
    protected String getFieldName(String n) {
        return n.toUpperCase();
    }

    @Override
    protected String getSelfName() {
        return "net/minecraft/init/Enchantments";
    }
}

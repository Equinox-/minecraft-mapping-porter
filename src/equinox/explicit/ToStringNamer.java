package equinox.explicit;

import equinox.expanders.DualExplicitMapper;
import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.ConstantString;
import org.apache.bcel.classfile.EmptyVisitor;
import org.apache.bcel.generic.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ToStringNamer extends DualExplicitMapper {
    @Override
    protected Map<String, Object> Compute(Fingerprint fp) {
        Map<String, Set<Class>> toStringMappers = new HashMap<>();
        for (Class c : fp.Classes()) {
            Method toString = c.GetMethod("toString", "()Ljava/lang/String;");
            if (toString == null)
                continue;
            Code code = toString.Backing.getCode();
            if (code == null)
                continue;
            InstructionList il = new InstructionList(code.getCode());
            ConstantPoolGen cpg = new ConstantPoolGen(code.getConstantPool());
            Instruction[] set = il.getInstructions();
            for (Instruction i : set) {
                if (i instanceof LDC ) {
                    LDC ldc = (LDC) i;
                    Object o = ldc.getValue(cpg);
                    if (o instanceof String) {
                        String res = (String) o;
                        int curly = res.indexOf('{');
                        if (curly > 0)
                            res = res.substring(0, curly);
                        Set<Class> ent = toStringMappers.getOrDefault(res, null);
                        if (ent==null)
                            toStringMappers.put(res, ent = new HashSet<>());
                        ent.add(c);
                        break;
                    }
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        for (Map.Entry<String, Set<Class>> res : toStringMappers.entrySet())
            if (res.getValue().size() == 1)
                map.put(res.getKey(), res.getValue().iterator().next());
        return map;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return false;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

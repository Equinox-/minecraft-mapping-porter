package equinox.explicit;

import equinox.expanders.DualExplicitMapper;
import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UniqueStringConstantClassNamer extends DualExplicitMapper {
    @Override
    protected Map<String, Object> Compute(Fingerprint fp) {
        Map<String, List<Class>> tmp = new HashMap<>();
        for (Class c : fp.Classes()) {
            for (String s : c.StringConstants())
                if (s.length() > 8) {
                    List<Class> r = tmp.get(s);
                    if (r == null)
                        tmp.put(s, r = new ArrayList<>());
                    r.add(c);
                }
        }
        Map<String, Object> res = new HashMap<>();
        for (Map.Entry<String, List<Class>> e : tmp.entrySet())
            if (e.getValue().size() == 1)
                res.put(e.getKey(), e.getValue().get(0));
        return res;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return false;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

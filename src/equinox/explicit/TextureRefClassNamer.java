package equinox.explicit;

import equinox.expanders.DualExplicitMapper;
import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;

import java.util.*;

public class TextureRefClassNamer extends DualExplicitMapper {
    @Override
    protected Map<String, Object> Compute(Fingerprint fp) {
        Map<String, Object> map = new HashMap<>();
        Map<String, Set<Class>> entries = new HashMap<>();
        for (Class c : fp.Classes()) {
            if (c.IsUnobfuscated)
                continue;
            Method clinit = null;
            for (Method m : c.Methods())
                if (m.isStaticCtor()) {
                    clinit = m;
                    break;
                }
            if (clinit == null)
                continue;
            Set<String> textureRefs = new HashSet<String>();
            for (String s : clinit.StringConstants())
                if (s.startsWith("textures/"))
                    textureRefs.add(s);
            if (textureRefs.size() > 0) {
                String key = textureRefs.toString();
                Set<Class> suppl = entries.getOrDefault(key, null);
                if (suppl == null)
                    entries.put(key, suppl = new HashSet<>());
                suppl.add(c);
                map.put(key, c);
            }
        }

        for (Map.Entry<String, Set<Class>> e : entries.entrySet())
            if (e.getValue().size() > 1) map.remove(e.getKey());
        return map;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return false;
    }
}

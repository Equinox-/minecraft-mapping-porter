package equinox.explicit;

public class StatePropertiesNamer extends DatabaseNamer {
    public StatePropertiesNamer() {
        super("attached",
                "conditional",
                "disarmed",
                "drag",
                "enabled",
                "extended",
                "eye",
                "falling",
                "has_bottle_0",
                "has_bottle_1",
                "has_bottle_2",
                "has_record",
                "inverted",
                "in_wall",
                "lit",
                "locked",
                "occupied",
                "open",
                "persistent",
                "powered",
                "short",
                "snowy",
                "triggered",
                "unstable",
                "waterlogged",
                "axis",
                "up",
                "down",
                "north",
                "east",
                "south",
                "west",
                "facing",
                "face",
                "half",
                "shape",
                "age",
                "bites",
                "delay",
                "distance",
                "eggs",
                "hatch",
                "layers",
                "level",
                "moisture",
                "note",
                "pickles",
                "power",
                "stage",
                "rotation",
                "part",
                "type",
                "mode",
                "hinge",
                "instrument",
                "leaves");
    }

    @Override
    protected String getTypeName(String n) {
        return "net/minecraft/state/properties/PropertyTypeFor" + n;
    }

    @Override
    protected String getFieldName(String n) {
        return "net/minecraft/state/properties/PropertyPresets#prop_" + n;
    }

    @Override
    protected String getSelfName() {
        return "net/minecraft/state/properties/PropertyPresets";
    }
}

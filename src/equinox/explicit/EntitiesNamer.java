package equinox.explicit;

public class EntitiesNamer extends DatabaseNamer {

    public EntitiesNamer() {
        super("area_effect_cloud",
                "armor_stand",
                "arrow",
                "bat",
                "blaze",
                "boat",
                "cave_spider",
                "chicken",
                "cod_mob",
                "cow",
                "creeper",
                "donkey",
                "dragon_fireball",
                "drowned",
                "elder_guardian",
                "ender_crystal",
                "ender_dragon",
                "enderman",
                "endermite",
                "evocation_fangs",
                "evocation_illager",
                "xp_orb",
                "eye_of_ender_signal",
                "falling_block",
                "fireworks_rocket",
                "ghast",
                "giant",
                "guardian",
                "horse",
                "husk",
                "illusion_illager",
                "item",
                "item_frame",
                "fireball",
                "leash_knot",
                "llama",
                "llama_spit",
                "magma_cube",
                "minecart",
                "chest_minecart",
                "commandblock_minecart",
                "furnace_minecart",
                "hopper_minecart",
                "spawner_minecart",
                "tnt_minecart",
                "mule",
                "mooshroom",
                "ocelot",
                "painting",
                "parrot",
                "pig",
                "puffer_fish",
                "zombie_pigman",
                "polar_bear",
                "tnt",
                "rabbit",
                "salmon_mob",
                "sheep",
                "shulker",
                "shulker_bullet",
                "silverfish",
                "skeleton",
                "skeleton_horse",
                "slime",
                "small_fireball",
                "snowman",
                "snowball",
                "spectral_arrow",
                "spider",
                "squid",
                "stray",
                "tropical_fish",
                "turtle",
                "egg",
                "ender_pearl",
                "xp_bottle",
                "potion",
                "vex",
                "villager",
                "villager_golem",
                "vindication_illager",
                "witch",
                "wither",
                "wither_skeleton",
                "wither_skull",
                "wolf",
                "zombie",
                "zombie_horse",
                "zombie_villager",
                "phantom",
                "lightning_bolt",
                "player",
                "fishing_bobber",
                "trident");
    }

    @Override
    protected String getTypeName(String n) {
        return "net/minecraft/entity/Entity" + tagToName(n);
    }

    @Override
    protected String getFieldName(String n) {
        return n.toUpperCase();
    }

    @Override
    protected String getSelfName() {
        return "net/minecraft/init/Entities";
    }
}

package equinox.explicit;

public class StructureNamer extends DatabaseNamer {
    public StructureNamer() {
        super("Mineshaft",
                "Village",
                "Fortress",
                "Stronghold",
                "Jungle_Pyramid",
                "Ocean_Ruin",
                "Desert_Pyramid",
                "Igloo",
                "Swamp_Hut",
                "Monument",
                "EndCity",
                "Mansion",
                "Buried_Treasure",
                "Shipwreck");
    }

    @Override
    protected String getTypeName(String n) {
        return "net/minecraft/gen/feature/structure/" + n.replace("_", "" + "$" + "Start");
    }

    @Override
    protected String getFieldName(String n) {
        return n;
    }

    @Override
    protected String getSelfName() {
        return null;
    }
}

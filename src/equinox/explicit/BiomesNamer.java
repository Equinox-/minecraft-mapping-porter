package equinox.explicit;

public class BiomesNamer extends DatabaseNamer {
    public BiomesNamer() {
        super("ocean",
                "plains",
                "desert",
                "mountains",
                "forest",
                "taiga",
                "swamp",
                "river",
                "nether",
                "the_end",
                "frozen_ocean",
                "frozen_river",
                "snowy_tundra",
                "snowy_mountains",
                "mushroom_fields",
                "mushroom_field_shore",
                "beach",
                "desert_hills",
                "wooded_hills",
                "taiga_hills",
                "mountain_edge",
                "jungle",
                "jungle_hills",
                "jungle_edge",
                "deep_ocean",
                "stone_shore",
                "snowy_beach",
                "birch_forest",
                "birch_forest_hills",
                "dark_forest",
                "snowy_taiga",
                "snowy_taiga_hills",
                "giant_tree_taiga",
                "giant_tree_taiga_hills",
                "wooded_mountains",
                "savanna",
                "savanna_plateau",
                "badlands",
                "wooded_badlands_plateau",
                "badlands_plateau",
                "small_end_islands",
                "end_midlands",
                "end_highlands",
                "end_barrens",
                "warm_ocean",
                "lukewarm_ocean",
                "cold_ocean",
                "deep_warm_ocean",
                "deep_lukewarm_ocean",
                "deep_cold_ocean",
                "deep_frozen_ocean",
                "the_void",
                "sunflower_plains",
                "desert_lakes",
                "gravelly_mountains",
                "flower_forest",
                "taiga_mountains",
                "swamp_hills",
                "ice_spikes",
                "modified_jungle",
                "modified_jungle_edge",
                "tall_birch_forest",
                "tall_birch_hills",
                "dark_forest_hills",
                "snowy_taiga_mountains",
                "giant_spruce_taiga",
                "giant_spruce_taiga_hills",
                "modified_gravelly_mountains",
                "shattered_savanna",
                "shattered_savanna_plateau",
                "eroded_badlands",
                "modified_wooded_badlands_plateau",
                "modified_badlands_plateau");
    }

    @Override
    protected String getTypeName(String n) {
        return "net/minecraft/world/biome/Biome" + tagToName(n);
    }

    @Override
    protected String getFieldName(String n) {
        return n.toUpperCase();
    }

    @Override
    protected String getSelfName() {
        return "net/minecraft/init/Biomes";
    }
}

package equinox.explicit;

import com.sun.org.apache.bcel.internal.Constants;
import equinox.expanders.DualExplicitMapper;
import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantString;
import org.apache.bcel.generic.*;

import java.util.*;
import java.util.stream.Collectors;

public abstract class DatabaseNamer extends DualExplicitMapper {

    private final Set<String> _tags;

    public static String tagToName(String tag) {
        StringBuilder res = new StringBuilder(tag.length());
        boolean cap = true;
        for (int i = 0; i < tag.length(); i++) {
            char c = tag.charAt(i);
            if (c == '_')
                cap = true;
            else {
                res.append(cap ? Character.toUpperCase(c) : c);
                cap = false;
            }
        }
        return res.toString();
    }

    protected DatabaseNamer(String... tags) {
        _tags = new HashSet<>();
        for (String s : tags)
            _tags.add(s);
    }

    protected abstract String getTypeName(String n);

    protected abstract String getFieldName(String n);

    protected abstract String getSelfName();

    @Override
    public Map<String, Object> Compute(Fingerprint fp) {
        Map<String, Object> keyValMap = new HashMap<String, Object>();

        AbstractMap.SimpleEntry<Class, Integer>[] results = StringFingerprint.FindClass(fp, _tags).toArray(x -> new AbstractMap.SimpleEntry[x]);
        if (results.length == 0) {
            System.out.println("Couldn't find type for " + getClass().getSimpleName());
            return keyValMap;
        }
//        if (results.length > 2) {
//            System.out.println("More than two type matches for " + getClass().getSimpleName());
//            for (AbstractMap.SimpleEntry<Class, Integer> res : results)
//                if (res.getValue() > 0)
//                    System.out.println("\t- " + res.getKey().Backing.getClassName() + " score=" + res.getValue());
//            return keyValMap;
//        }

        Map<Class, String> typeNameMapping = new HashMap<>();
        Map<Field, String> fieldNameMapping = new HashMap<>();

        List<Map.Entry<Class, Integer>> initTypes = new ArrayList<>();
        for (AbstractMap.SimpleEntry<Class, Integer> resEntry : results) {
            Class scanType = resEntry.getKey();
            int fieldMatchThis = 0;
            Method scanMethod = null;
            int bestScore = Integer.MIN_VALUE;
            for (Method m : scanType.Methods()) {
                int score = 0;
                for (String s : _tags)
                    if (m.HasStringConstant(s))
                        score++;
                if (score > bestScore) {
                    bestScore = score;
                    scanMethod = m;
                }
            }
            if (scanMethod == null)
                continue;
//            System.out.println(getClass().getSimpleName() + " matches " + scanMethod + " score=" + bestScore);
            String activeKeyForString = null;
            String activeKeyForType = null;

            ConstantPoolGen cpg = new ConstantPoolGen(scanMethod.Backing.getConstantPool());
            // look for...
            // field = invoke(string)
            // register("string", [int,] Type)
            for (InstructionHandle ih : new InstructionList(scanMethod.Backing.getCode().getCode())) {
                Instruction i = ih.getInstruction();
                if ((i instanceof PushInstruction || i instanceof NEW) && i instanceof CPInstruction) {
                    CPInstruction cpi = (CPInstruction) i;
                    Constant cst = scanMethod.Backing.getConstantPool().getConstant(cpi.getIndex());
                    if (cst instanceof ConstantString) {
                        activeKeyForString = ((ConstantString) cst).getConstantValue(scanMethod.Backing.getConstantPool()).toString();
                    } else if (cst instanceof ConstantClass) {
                        ConstantClass tref = (ConstantClass) cst;
                        activeKeyForType = tref.getConstantValue(scanMethod.Backing.getConstantPool()).toString();
                    }
                } else if (i.getOpcode() == Constants.PUTSTATIC) {
                    PUTSTATIC ips = (PUTSTATIC) i;
                    String fieldName = ips.getFieldName(cpg);
                    Type tt = ips.getFieldType(cpg);
                    ReferenceType rt = ips.getReferenceType(cpg);
                    if (rt instanceof ObjectType && tt instanceof ObjectType) {
                        ObjectType ot = (ObjectType) rt;
                        ObjectType ott = (ObjectType) tt;
                        if (ot.getClassName().equals(scanType.Backing.getClassName()) && fp.IsMinecraft(ott.getClassName())) {
                            if (activeKeyForString != null || activeKeyForType != null) {
                                equinox.fingerprint.Field fieldHandle = fp.GetClass(ot.getClassName()).GetField(fieldName);
                                if (fieldNameMapping.containsKey(fieldHandle))
                                    throw new RuntimeException(fieldHandle + " was duplicated on " + scanType.Backing.getClass());
                                fieldMatchThis++;
                                if (activeKeyForType != null)
                                    fieldNameMapping.put(fieldHandle, getFieldName(activeKeyForType));
                                else
                                    fieldNameMapping.put(fieldHandle, getFieldName(activeKeyForString));
                                activeKeyForString = activeKeyForType = null;
                            }
                        }
                    }
                } else if (i.getOpcode() == Constants.INVOKESTATIC) {
                    InvokeInstruction call = (InvokeInstruction) i;
                    Type[] args = call.getArgumentTypes(cpg);
                    if ((args.length >= 2 && args[0].equals(Type.STRING) && args[1] instanceof ObjectType) // string keys
                            || (args.length == 3 && args[0].equals(Type.INT) && args[1].equals(Type.STRING) && args[2] instanceof ObjectType) // int keys (biomes and mob effects)
                            || (args.length == 2 && args[0] instanceof ObjectType && args[1].equals(Type.STRING) && ((ObjectType) args[0]).getClassName().equals("java.lang.Class"))) // structure
                    {
                        if (activeKeyForString != null && activeKeyForType != null) {
                            typeNameMapping.put(fp.GetClass(activeKeyForType), activeKeyForString);
//                            activeKeyForString = activeKeyForType = null;
                        }
                    }
                }
            }

            if (fieldMatchThis > 0)
                initTypes.add(new AbstractMap.SimpleEntry<>(scanType, fieldMatchThis));
        }

        initTypes.sort(Comparator.comparingInt(x -> -x.getValue()));

        for (Map.Entry<Class, String> ep : typeNameMapping.entrySet())
            keyValMap.put(getTypeName(ep.getValue()), ep.getKey());

        if (getSelfName() != null) {
            for (Map.Entry<Field, String> ep : fieldNameMapping.entrySet())
                if (initTypes.size() == 0 || initTypes.get(0).getKey() == ep.getKey().Parent)
                    keyValMap.put(getSelfName() + "#" + getFieldName(ep.getValue()), ep.getKey());
                else
                    keyValMap.put(getSelfName() + "_Extra_" + +ep.getKey().Parent.hashCode() + "#" + getFieldName(ep.getValue()), ep.getKey());

            if (initTypes.size() > 0 && (initTypes.size() == 1 || initTypes.get(0).getValue() > initTypes.get(1).getValue() * 4))
                keyValMap.put(getSelfName(), initTypes.get(0).getKey());
            else
                System.err.println("Irregular appearance of " + getSelfName() + "\t" + initTypes);
        }
        // Remove multi-type entries
        Map<Object, String> reverse = new HashMap<>();
        for (Map.Entry<String, Object> f : keyValMap.entrySet().stream().collect(Collectors.toList())) {
            String other = reverse.get(f.getValue());
            if (reverse.containsKey(f.getValue())) {
                keyValMap.remove(f.getKey());
                keyValMap.remove(other);
                continue;
            }
            reverse.put(f.getValue(), f.getKey());
        }
        return keyValMap;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return true;
    }
}

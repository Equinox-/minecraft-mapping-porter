package equinox.explicit;

public class MobEffectNamer extends DatabaseNamer {
    public MobEffectNamer() {
        super("speed",
                "slowness",
                "haste",
                "mining_fatigue",
                "strength",
                "instant_health",
                "instant_damage",
                "jump_boost",
                "nausea",
                "regeneration",
                "resistance",
                "fire_resistance",
                "water_breathing",
                "invisibility",
                "blindness",
                "night_vision",
                "hunger",
                "weakness",
                "poison",
                "wither",
                "health_boost",
                "absorption",
                "saturation",
                "glowing",
                "levitation",
                "luck",
                "unluck");
    }

    @Override
    protected String getTypeName(String n) {
        return "net/minecraft/potion/" + tagToName(n);
    }

    @Override
    protected String getFieldName(String n) {
        return n.toUpperCase();
    }

    @Override
    protected String getSelfName() {
        return "net/minecraft/init/MobEffects";
    }
}

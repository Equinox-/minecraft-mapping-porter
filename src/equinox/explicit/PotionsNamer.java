package equinox.explicit;

public class PotionsNamer extends DatabaseNamer{
    public PotionsNamer() {
        super("empty",
                "water",
                "mundane",
                "thick",
                "awkward",
                "night_vision",
                "long_night_vision",
                "invisibility",
                "long_invisibility",
                "leaping",
                "long_leaping",
                "strong_leaping",
                "fire_resistance",
                "long_fire_resistance",
                "swiftness",
                "long_swiftness",
                "strong_swiftness",
                "slowness",
                "long_slowness",
                "water_breathing",
                "long_water_breathing",
                "healing",
                "strong_healing",
                "harming",
                "strong_harming",
                "poison",
                "long_poison",
                "strong_poison",
                "regeneration",
                "long_regeneration",
                "strong_regeneration",
                "strength",
                "long_strength",
                "strong_strength",
                "weakness",
                "long_weakness");
    }


    @Override
    protected String getTypeName(String n) {
        return "net/minecraft/potion/Potion" + tagToName(n);
    }

    @Override
    protected String getFieldName(String n) {
        return n.toUpperCase();
    }

    @Override
    protected String getSelfName() {
        return "net/minecraft/init/Potions";
    }
}

package equinox.explicit;

import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;
import org.apache.bcel.classfile.ConstantString;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StringFingerprint {
    public static Stream<AbstractMap.SimpleEntry<Class, Integer>> FindClass(Fingerprint fp, Set<String> table) {
        List<AbstractMap.SimpleEntry<Class, Integer>> lst = StreamSupport.stream(fp.Classes().spliterator(), false).map(clazz -> {
            Set<String> cst = Arrays.stream(clazz.Backing.getConstantPool().getConstantPool()).filter(y -> y instanceof ConstantString).map(y -> ((ConstantString) y).getConstantValue(clazz.Backing.getConstantPool()).toString()).collect(Collectors.toSet());
            int error = 0;
            for (String test : table)
                if (cst.contains(test))
                    error++;
            return new AbstractMap.SimpleEntry<>(clazz, error);
        }).sorted((a, b) -> Integer.compare(b.getValue(), a.getValue())).collect(Collectors.toList());
        if (lst.size() == 0)
            return null;
        if (lst.get(0).getValue() == 0)
            throw new RuntimeException("No matches");
        if (lst.size() == 1)
            return Stream.of(lst.get(0));
        int k = lst.get(0).getValue();
        return lst.stream().filter(x -> x.getValue() >= k / 2);
    }
}

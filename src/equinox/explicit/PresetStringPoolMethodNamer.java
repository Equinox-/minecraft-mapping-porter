package equinox.explicit;

import equinox.expanders.DualExplicitMapper;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;

import java.util.*;
import java.util.stream.Collectors;

public class PresetStringPoolMethodNamer extends DualExplicitMapper {
    private static final Map<String, String[]> _keyToPool = new HashMap<>();

    private static void register(String key, String... vals) {
        _keyToPool.put(key, vals);
    }

    static {
        register("EntityAddPassenger", "Use x.startRiding(y), not y.addPassenger(x)");
        register("EntityRemovePassenger", "Use x.stopRiding(y), not y.removePassenger(x)");
        register("CheckSessionLock", "The save is being accessed from another location, aborting");
        register("ParseRecipeFromJson", "An ingredient entry needs either a tag or an item");
        register("GetCWFacingAxis", "Unable to get CW facing for axis ");
        register("GetYRotatedFacing", "Unable to get Y-rotated facing of ");
        register("GetXRotatedFacing", "Unable to get X-rotated facing of ");
        register("GetZRotatedFacing", "Unable to get Z-rotated facing of ");
        register("GetCCWFacingAxis", "Unable to get CCW facing of ");
        register("CtorFollowOwnerGoal", "Unsupported mob type for FollowOwnerGoal");
        register("CtorDoorInteractGoal", "Unsupported mob type for DoorInteractGoal");
        register("CtorFollowMobGoal", "Unsupported mob type for FollowMobGoal");
        register("CtorMoveThroughVillageGoal", "Unsupported mob for MoveThroughVillageGoal");
        register("CtorArrowAttackGoal", "ArrowAttackGoal requires Mob implements RangedAttackMob");
        register("CtorRestrictOpenDoorGoal", "Unsupported mob type for RestrictOpenDoorGoal");
        register("CtorTemptGoal", "Unsupported mob type for TemptGoal");
        register("BiomeRequest", "Invalid Biome requested: ");
        register("TagListSetIndex", "index out of bounds to set tag in tag list", "Adding mismatching tag types to tag list");
        register("DataValueSynchStor", "Data value id is too big with ", "Duplicate id value for ", "Unregistered serializer ");
        register("ResourceLocationToPath", ".unregistered_sadface");
        register("UtilGetOSType", "win", "mac", "solaris", "sunos", "linux", "unix");
        register("AdvancementListRemoveAll", "Told to remove advancement {} but I don't know what that is");
        register("RegisterPacket", " is already known to ID ");
        register("PacketDecode", "Bad packet id ");
        register("PacketDecodev2", "Can't serialize unregistered packet");
        register("CtorLocalization", "/assets/minecraft/lang/en_us.json", "Couldn't read strings from /assets/minecraft/lang/en_us.json");
        register("RegisterSerializer", "' is already a registered serializer!", " already has a serializer!");
        register("DeserializeAttributeModifier", "Invalid or missing attribute modifier slot; must be either string or array of strings.",
                "Invalid attribute modifier slot; must contain at least one entry.");
        register("RegisterItemFunction", "Can't re-register item function name ", "Can't re-register item function class ");
        register("GetCompatibleEnchants", "Couldn't find a compatible enchantment for {}");
        register("SerializeEnchant", "Don't know how to serialize enchantment ");
        register("WriteCommandTree", "Couldn't write out command tree!");
    }

    public static Method FindGroup(Fingerprint fp, String... find) {
        if (find.length == 0)
            return null;
        Set<Method> explored = new HashSet<>();
        List<Method> groupKeys = new ArrayList<>();
        List<Set<Method>> groups = new ArrayList<>();
        for (Method m : fp.MethodsWithConstant(find[0], true)) {
            if (explored.contains(m))
                continue;
            boolean success = true;
            for (String s : find)
                if (!m.HasStringConstant(s)) {
                    success = false;
                    break;
                }
            if (!success)
                continue;
            Set<Method> group = m.Group();
            groups.add(group);
            groupKeys.add(m);
            for (Method mem : group)
                explored.add(mem);
        }

        if (groups.size() != 1) {
            System.out.println("Failed to find group by string constants " + Arrays.stream(find).collect(Collectors.toList()));
            for (Set<Method> g : groups)
                System.out.println("\t- " + g);
            return null;
        }
        return groupKeys.get(0);
    }

    @Override
    protected Map<String, Object> Compute(Fingerprint fp) {
        Map<String, Object> result = new HashMap<>();
        for (Map.Entry<String, String[]> ent : _keyToPool.entrySet()) {
            Method found = FindGroup(fp, ent.getValue());
            if (found != null)
                result.put(ent.getKey(), found);
        }
        return result;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return false;
    }
}

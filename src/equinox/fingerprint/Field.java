package equinox.fingerprint;

import equinox.remap.IRemappedElement;
import equinox.util.GenericAwareType;

public class Field implements IRemappedElement {
    public final Class Parent;
    public final org.apache.bcel.classfile.Field Backing;
    public final GenericAwareType Type;

    public Field(Class parent, org.apache.bcel.classfile.Field data) {
        Parent = parent;
        Backing = data;
        String sig = data.getGenericSignature();
        if (sig == null)
            sig = data.getSignature();
        Type = GenericAwareType.Decode(sig);
    }

    @Override
    public String getObfuscatedName() {
        return Backing.getName();
    }

    @Override
    public String getRemappedName() {
        return Parent.Parent.Remap.RemappedName(this);
    }

    @Override
    public String toString() {
        return Parent.Backing.getClassName() + "#" + Backing.getName() + (Parent.isRemapped() || isRemapped() ? (" (" + Parent.getRemappedName() + "#" + getRemappedName() + ")") : "");
    }
}

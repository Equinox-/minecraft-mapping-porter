package equinox.fingerprint;

public interface ICompareResult {
    double getError();
}

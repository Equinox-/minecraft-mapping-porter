package equinox.fingerprint;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class StableMarriage<T extends IComparableMapping<T>> {
    private final Man[] _men;
    private final Woman[] _women;
    private final boolean _progress;


    public static <T extends IComparableMapping<T>> double Compute(T[] a, T[] b,  Map<T, Map.Entry<T, ICompareResult>> remapAToB) {
        return Compute(a, b,  remapAToB, false);
    }

    public static <T extends IComparableMapping<T>> double Compute(T[] a, T[] b, Map<T, Map.Entry<T, ICompareResult>> remapAToB, boolean progress) {
        if (a.length < b.length) {
            StableMarriage<T> mg = new StableMarriage(a, b,  progress);
            Map.Entry<T, ICompareResult>[] result = (Map.Entry<T, ICompareResult>[]) Array.newInstance(Map.Entry.class, a.length);
            double err = mg.Match(result);
            for (int i = 0; i < a.length; i++)
                remapAToB.put(a[i], result[i]);
            return err;
        } else {
            StableMarriage<T> mg = new StableMarriage(b, a,  progress);
            Map.Entry<T, ICompareResult>[] result = (Map.Entry<T, ICompareResult>[]) Array.newInstance(Map.Entry.class, b.length);
            double err = mg.Match(result);
            for (int i = 0; i < b.length; i++)
                remapAToB.put(result[i].getKey(), new AbstractMap.SimpleEntry<>(b[i], result[i].getValue()));
            return err;
        }
    }


    public StableMarriage(T[] men, T[] women,  boolean progress) {
        if (women.length < men.length)
            throw new RuntimeException("Reverse the args");

        _women = (Woman[]) Array.newInstance(Woman.class, women.length);
        for (int i = 0; i < _women.length; i++)
            _women[i] = new Woman(women[i]);
        AtomicInteger count = new AtomicInteger();
        final int divide = men.length / 10;
        _men = Arrays.stream(men).parallel().map(man -> {
            int sofar = count.incrementAndGet();
            if (progress && (sofar % divide) == 0)
                System.out.println("Progress " + sofar + "/" + men.length + " (" + (100.0 * sofar / men.length) + " %)");
            return new Man(man, _women);
        }).toArray(len -> (Man[]) Array.newInstance(Man.class, len));

        _progress = progress;
    }

    public double Match(Map.Entry<T, ICompareResult>[] result) {
        while (true) {
            boolean modified = false;
            for (Man man : _men) {
                if (man.Proposed >= man.Preferences.length || man.Suitor != null)
                    continue;
                Pair pref = man.Preferences[man.Proposed];
                man.Proposed++;
                pref.a.TryEngage(man, pref.b);
                modified = true;
            }
            if (!modified)
                break;
        }
        double error = 0;
        for (int i = 0; i < _men.length; i++) {
//            if (_progress && _men[i].Suitor != _men[i].Preferences[0].a) {
//                System.out.println(_men[i].Self + " got " + _men[i].Suitor.Self + " (" + _men[i].SuitorResult.getError() + ")" + " but wanted " + _men[i].Preferences[0].a.Self
//                        + " (" + _men[i].Preferences[0].b.getError() + ")");
//            }
            error += _men[i].SuitorResult.getError();
            result[i] = new AbstractMap.SimpleEntry<>(_men[i].Suitor.Self, _men[i].SuitorResult);
        }
        return error;
    }

    private class Man {
        public final T Self;
        public final Pair[] Preferences;
        public int Proposed;

        public Man(T value, Woman[] others) {
            Self = value;
            Preferences = Preference(this, others);
//            System.out.println("Best for " + value + " is "+ Preferences[0].a.Self + "\t" + Preferences[0].b.getError());
            Proposed = 0;
        }

        public Woman Suitor;
        public ICompareResult SuitorResult;
    }

    private class Woman {
        public final T Self;

        public Woman(T self) {
            Self = self;
        }

        public Man Suitor;
        public ICompareResult SuitorResult;

        public boolean TryEngage(Man suitor, ICompareResult result) {
            if (Suitor != null) {
                if (SuitorResult.getError() < result.getError())
                    return false;
                Suitor.Suitor = null;
                Suitor.SuitorResult = null;
            }
            Suitor = suitor;
            SuitorResult = result;
            Suitor.Suitor = this;
            Suitor.SuitorResult = result;
            return true;
        }
    }

    private Pair[] Preference(Man a, Woman[] b) {
        Pair[] results = Arrays.stream(b).parallel().map(woman -> new Pair(woman, a.Self.compareTo(woman.Self)))
                .sorted(Comparator.comparingDouble(o -> o.b.getError()))
                .toArray(len -> (Pair[]) Array.newInstance(Pair.class, len));
        return results;
    }

    private class Pair {
        public final Woman a;
        public final ICompareResult b;

        public Pair(Woman a, ICompareResult b) {
            this.a = a;
            this.b = b;
        }
    }

}

package equinox.fingerprint;

public interface IComparableMapping<T> {
    ICompareResult compareTo(T self);
}

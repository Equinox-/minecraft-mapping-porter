package equinox.fingerprint;

import java.util.*;

public class Types {
    public static <T1, T2> Map<T1, Map.Entry<T2, ICompareResult>> Flip(Map<T2, Map.Entry<T1, ICompareResult>> input) {
        Map<T1,  Map.Entry<T2, ICompareResult>> res = new HashMap<>(input.size());
        for (Map.Entry<T2, Map.Entry<T1, ICompareResult>> tk : input.entrySet())
            res.put(tk.getValue().getKey(), new AbstractMap.SimpleEntry<T2, ICompareResult>(tk.getKey(), tk.getValue().getValue()));
        return res;
    }

    private static final Set<String> _keywords = new HashSet<>();

    public static boolean isKeyword(String s) {
        return _keywords.contains(s);
    }

    static {
        _keywords.add("abstract");
        _keywords.add("continue");
        _keywords.add("for");
        _keywords.add("new");
        _keywords.add("switch");
        _keywords.add("assert");
        _keywords.add("default");
        _keywords.add("goto");
        _keywords.add("package");
        _keywords.add("synchronized");
        _keywords.add("boolean");
        _keywords.add("do");
        _keywords.add("if");
        _keywords.add("private");
        _keywords.add("this");
        _keywords.add("break");
        _keywords.add("double");
        _keywords.add("implements");
        _keywords.add("protected");
        _keywords.add("throw");
        _keywords.add("byte");
        _keywords.add("else");
        _keywords.add("import");
        _keywords.add("public");
        _keywords.add("throws");
        _keywords.add("case");
        _keywords.add("enum");
        _keywords.add("instanceof");
        _keywords.add("return");
        _keywords.add("transient");
        _keywords.add("catch");
        _keywords.add("extends");
        _keywords.add("int");
        _keywords.add("short");
        _keywords.add("try");
        _keywords.add("char");
        _keywords.add("final");
        _keywords.add("interface");
        _keywords.add("static");
        _keywords.add("void");
        _keywords.add("class");
        _keywords.add("finally");
        _keywords.add("long");
        _keywords.add("strictfp");
        _keywords.add("volatile");
        _keywords.add("const");
        _keywords.add("float");
        _keywords.add("native");
        _keywords.add("super");
        _keywords.add("while");
    }
}

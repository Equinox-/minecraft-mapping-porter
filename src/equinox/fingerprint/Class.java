package equinox.fingerprint;

import equinox.remap.IRemappedElement;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantString;
import org.apache.bcel.classfile.JavaClass;

import java.util.*;

public class Class implements IRemappedElement {
    public final JavaClass Backing;
    public final Set<Method> _methods = new HashSet<>();
    public final Fingerprint Parent;
    public final Set<Class> _implementors = new HashSet<>();
    public final Set<Class> _nestedClasses = new HashSet<>();
    public final Set<String> BaseTypes = new HashSet<>();
    public final Set<Class> DerivedTypes = new HashSet<>();
    private final Map<String, Field> _fields = new HashMap<>();
    private final Set<String> _stringConstants = new HashSet<>();

    public Class NestedFor;

    public final boolean IsMinecraft;

    public final boolean IsUnobfuscated;

    public Class(Fingerprint parent, JavaClass clazz, boolean isEmbedded) {
        Parent = parent;

        IsMinecraft = isEmbedded;
        IsUnobfuscated = !IsMinecraft || clazz.getClassName().contains("/");
        Backing = clazz;
    }

    public Iterable<Field> Fields() {
        return _fields.values();
    }

    public Field GetField(String name) {
        return _fields.get(name);
    }

    public Iterable<Method> Methods() {
        return _methods;
    }

    public Method GetMethod(String name, String signature) {
        for (Method m : _methods)
            if (m.Backing.getName().equals(name) && m.Backing.getSignature().equals(signature))
                return m;
        return null;
    }

    public Method GetImplementation(Method other) {
        for (Method m : _methods)
            if (m.Overrides(other))
                return m;
        return null;
    }

    private boolean _isLoaded;

    public void EnsureInheritance() {
        if (_isLoaded)
            return;
        _isLoaded = true;
        for (String s : Backing.getInterfaceNames())
            BaseTypes.add(s);
        BaseTypes.add(Backing.getSuperclassName());
        BaseTypes.remove(Backing.getClassName());
        for (String s : BaseTypes)
            Parent.GetClass(s).DerivedTypes.add(this);
    }

    public boolean InheritsFrom(String clazz) {
        return Parent.AInheritsFromB(getObfuscatedName(), clazz);
    }

    private boolean _isMethodsLoaded;

    public void EnsureMethods() {
        if (_isMethodsLoaded)
            return;
        EnsureInheritance();
        _isMethodsLoaded = true;
        for (org.apache.bcel.classfile.Method m : Backing.getMethods()) {
            Method mt = new Method(this, m);
            _methods.add(mt);
            mt.Load();
        }

        _stringConstants.clear();
        for (Constant cst : Backing.getConstantPool().getConstantPool()) {
            if (cst instanceof ConstantString) {
                ConstantString cs = (ConstantString) cst;
                _stringConstants.add(cs.getConstantValue(Backing.getConstantPool()).toString());
            }
        }
        for (org.apache.bcel.classfile.Method m : Backing.getMethods())
            for (Constant cst : m.getConstantPool().getConstantPool())
                if (cst instanceof ConstantString) {
                    ConstantString cs = (ConstantString) cst;
                    _stringConstants.add(cs.getConstantValue(Backing.getConstantPool()).toString());
                }

        _fields.clear();
        for (org.apache.bcel.classfile.Field k : Backing.getFields())
            _fields.put(k.getName(), new Field(this, k));
    }

    public Iterable<String> StringConstants() {
        return _stringConstants;
    }

    public Iterable<Class> NestedTypes() {
        return _nestedClasses;
    }

    public boolean HasStringConstant(String cst) {
        return _stringConstants.contains(cst);
    }

    public String getNestingAwareObfuscatedName() {
        String s = getObfuscatedName();
        return s.substring(s.lastIndexOf('$') + 1);
    }

    public String getNestingAwareRemappedName() {
        String s = getRemappedName();
        return s.substring(s.lastIndexOf('$') + 1);
    }

    @Override
    public String getObfuscatedName() {
        return Backing.getClassName();
    }

    @Override
    public String getRemappedName() {
        return Parent.Remap.RemappedName(this);
    }

    @Override
    public String toString() {
        return Backing.getClassName() + (isRemapped() ? " (" + getRemappedName() + ")" : "");
    }

    public boolean isSynthetic() {
        return Backing.isSynthetic() || (NestedFor != null && NestedFor.isSynthetic());
    }
}

package equinox.fingerprint;


import equinox.remap.IRemappedElement;
import org.apache.bcel.Const;
import org.apache.bcel.classfile.*;
import org.apache.bcel.classfile.EmptyVisitor;
import org.apache.bcel.generic.*;
import org.apache.bcel.generic.FieldOrMethod;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Method implements IRemappedElement {
    private static final Set<String> _reservedMethodNames = new HashSet<>();

    static {
        _reservedMethodNames.add("values");
        _reservedMethodNames.add("valueOf");
        _reservedMethodNames.add("equals");
        _reservedMethodNames.add("hashCode");
        _reservedMethodNames.add("finalize");
        _reservedMethodNames.add("clone");
        _reservedMethodNames.add("toString");
    }

    public final org.apache.bcel.classfile.Method Backing;

    public final Class Parent;
    private final Set<String> _constants = new HashSet<>();
    private final Set<String> _stringConstants = new HashSet<>();
    private final Set<String> _exceptionTable = new HashSet<>();
    private final Set<String> _references = new HashSet<>();

    public final Set<Map.Entry<JavaClass, org.apache.bcel.classfile.Field>> _staticFieldWrite = new HashSet<>();
    public final Set<Map.Entry<JavaClass, org.apache.bcel.classfile.Field>> _staticFieldRead = new HashSet<>();
    public final Set<Map.Entry<JavaClass, org.apache.bcel.classfile.Field>> _instanceFieldWrite = new HashSet<>();
    public final Set<Map.Entry<JavaClass, org.apache.bcel.classfile.Field>> _instanceFieldRead = new HashSet<>();
    private final Set<Method> _overriders = new HashSet<>();
    private final Set<Method> _overrides = new HashSet<>();

    private boolean _restricted;

    public boolean isStaticCtor() {
        return Backing.getName().equals("<clinit>");
    }

    public boolean isCtor() {
        return Backing.getName().equals("<init>");
    }

    public final boolean IsUnobfuscated;

    public Method(Class parent, org.apache.bcel.classfile.Method m) {
        Parent = parent;
        Backing = m;
        IsUnobfuscated = _reservedMethodNames.contains(m.getName()) || isStaticCtor() || isCtor() || isSynthetic();
    }

    public boolean isSynthetic() {
        return Backing.isSynthetic() || Parent.isSynthetic();
    }

    public boolean Overrides(Method m) {
        EnsureOverrideTree();
        return _overrides.contains(m);
    }

    @Override
    public String getObfuscatedName() {
        return Backing.getName();
    }

    @Override
    public String getRemappedName() {
        return Parent.Parent.Remap.RemappedName(this);
    }

    public Set<Method> Group() {
        Set<Method> methods = new HashSet<>();
        if (isStaticCtor() || isCtor() || _reservedMethodNames.contains(Backing.getName())) {
            methods.add(this);
        } else {
            Stack<Method> explore = new Stack<>();
            explore.push(this);
            while (explore.size() > 0) {
                Method test = explore.pop();
                if (!methods.add(test))
                    continue;
                test.EnsureOverrideTree();
                for (Method a : test._overriders)
                    explore.push(a);
                for (Method b : test._overrides)
                    explore.push(b);
            }
        }
        return methods;
    }

    public Method GroupRoot() {
        EnsureOverrideTree();
        Method root = this;
        while (root._overrides.size() > 0)
            root = root._overrides.iterator().next();
        return root;
    }

    private boolean _overrideTree = false;

    private void EnsureOverrideTree() {
        if (_overrideTree)
            return;
        _overrideTree = true;
        if (!Backing.isStatic()) {
            for (String baseName : Parent.BaseTypes) {
                Class baseType = Parent.Parent.GetClass(baseName);
                baseType.EnsureMethods();
                Method orig = baseType.GetMethod(Backing.getName(), Backing.getSignature());
                if (orig != null && !orig.Backing.isStatic()) {
                    orig._overriders.add(this);
                    _overrides.add(orig);
                }
            }
            for (Class derived : Parent.DerivedTypes) {
                derived.EnsureMethods();
                Method orig = derived.GetMethod(Backing.getName(), Backing.getSignature());
                if (orig != null && !orig.Backing.isStatic()) {
                    orig._overrides.add(this);
                    _overriders.add(orig);
                }
            }
        }
    }

    public void Load() {
        ExceptionTable exceptions = Backing.getExceptionTable();
        if (exceptions != null)
            for (String ex : exceptions.getExceptionNames())
                _exceptionTable.add(ex);
        _restricted = !Backing.isStatic() || Backing.isPrivate() || Backing.isProtected();
    }

    private boolean _analyzedIl = false;

    public void EnsureILAnalysis() {
        if (!_analyzedIl)
            AnalyzeIL();
    }

    private int _codeSize;

    public int GetCodeSize() {
        EnsureILAnalysis();
        return _codeSize;
    }

    private void AnalyzeIL() {
        _analyzedIl = true;
        Code code = Backing.getCode();
        if (code != null) {
            _codeSize = code.getCode().length;
        } else {
            _codeSize = -1;
        }

        EmptyVisitor visitor = new EmptyVisitor() {
            @Override
            public void visitConstantString(ConstantString obj) {
                _stringConstants.add(obj.getConstantValue(Backing.getConstantPool()).toString());
            }

            @Override
            public void visitConstantDouble(ConstantDouble obj) {
                _constants.add(String.format("float_%.2f", obj.getConstantValue(Backing.getConstantPool())));
            }

            @Override
            public void visitConstantFloat(ConstantFloat obj) {
                _constants.add(String.format("float_%.2f", obj.getConstantValue(Backing.getConstantPool())));
            }

            @Override
            public void visitConstantInteger(ConstantInteger obj) {
                _constants.add(String.format("int_%d", obj.getConstantValue(Backing.getConstantPool())));
            }

            @Override
            public void visitConstantLong(ConstantLong obj) {
                _constants.add(String.format("int_%d", obj.getConstantValue(Backing.getConstantPool())));
            }

            @Override
            public void visitConstantClass(ConstantClass obj) {
                visitType(obj.getConstantValue(Backing.getConstantPool()).toString());
            }

            @Override
            public void visitConstantFieldref(ConstantFieldref obj) {
                visitType(obj.getClass(Backing.getConstantPool()));
            }

            @Override
            public void visitConstantInterfaceMethodref(ConstantInterfaceMethodref obj) {
                visitType(obj.getClass(Backing.getConstantPool()));
            }

            @Override
            public void visitConstantMethodref(ConstantMethodref obj) {
                visitType(obj.getClass(Backing.getConstantPool()));
            }

            private void visitType(String refType) {
                int arrayEnd = refType.lastIndexOf('[');
                if (arrayEnd != -1) {
                    if (refType.charAt(arrayEnd + 1) != 'L')
                        return;
                    String elementType = refType.substring(arrayEnd + 2, refType.length() - 1);
                    visitType(elementType);
                    return;
                }
                _references.add(refType);
            }
        };

        if (code != null) {
            _staticFieldRead.clear();
            _staticFieldWrite.clear();
            _instanceFieldRead.clear();
            _instanceFieldWrite.clear();
            InstructionList il = new InstructionList(code.getCode());
            ConstantPoolGen cpg = new ConstantPoolGen(code.getConstantPool());
            for (InstructionHandle ih : il) {
                Instruction i = ih.getInstruction();
                if (i instanceof CPInstruction) {
                    CPInstruction cpi = (CPInstruction) i;
                    Constant cst = code.getConstantPool().getConstant(cpi.getIndex());
                    if (cst != null)
                        cst.accept(visitor);
                }
                if (i instanceof FieldOrMethod) {
                    FieldOrMethod fm = (FieldOrMethod) i;
                    ReferenceType refType = fm.getReferenceType(cpg);
                    if (refType.toString().contains(".") || refType.toString().contains("["))
                        continue;
                    try {
                        JavaClass lcg = Parent.Parent._repo.findClass(refType.toString());
                        if (i instanceof FieldInstruction) {
                            FieldInstruction fi = (FieldInstruction) i;
                            String nam = fi.getFieldName(cpg);
                            for (org.apache.bcel.classfile.Field f : lcg.getFields()) {
                                if (f.getName().equals(nam)) {
                                    Map.Entry<JavaClass, org.apache.bcel.classfile.Field> entry = new AbstractMap.SimpleEntry<>(lcg, f);
                                    if (i.getOpcode() == Const.PUTSTATIC)
                                        _staticFieldWrite.add(entry);
                                    else if (i.getOpcode() == Const.GETSTATIC)
                                        _staticFieldRead.add(entry);
                                    else if (i.getOpcode() == Const.PUTFIELD)
                                        _instanceFieldWrite.add(entry);
                                    else if (i.getOpcode() == Const.GETFIELD)
                                        _instanceFieldRead.add(entry);
                                    if (f.isPrivate())
                                        _restricted = true;
                                    break;
                                }
                            }
                        } else if (i instanceof InvokeInstruction) {
                            InvokeInstruction ii = (InvokeInstruction) i;
                            String sig = ii.getSignature(cpg);
                            String mnam = ii.getMethodName(cpg);
                            for (org.apache.bcel.classfile.Method m : lcg.getMethods()) {
                                if (m.getName().equals(mnam) && m.getSignature().equals(sig)) {
                                    if (m.isPrivate())
                                        _restricted = true;
                                    break;
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    public Iterable<String> StringConstants() {
        EnsureILAnalysis();
        return _stringConstants;
    }

    public boolean HasStringConstant(String cst) {
        EnsureILAnalysis();
        return _stringConstants.contains(cst);
    }

    public boolean HasConstant(String cst) {
        EnsureILAnalysis();
        return _constants.contains(cst) || _stringConstants.contains(cst);
    }

    public static int MatchBad(Set<String> ar, Function<String, String> selectorA,
                               Set<String> br, Function<String, String> selectorB) {
        return MatchBad(ar, selectorA, br, selectorB, false);
    }

    public static int MatchBad(Set<String> ar, Function<String, String> selectorA,
                               Set<String> br, Function<String, String> selectorB, boolean ignoreSizeDifference) {
        if (ar.size() > br.size())
            return MatchBad(br, selectorB, ar, selectorA, ignoreSizeDifference);

        Set<String> a = selectorA == null ? ar : ar.stream().map(selectorA).filter(x -> x != null).collect(Collectors.toSet());
        Set<String> b = selectorB == null ? br : br.stream().map(selectorB).filter(x -> x != null).collect(Collectors.toSet());
        int bad = 0;
        if (ignoreSizeDifference) {
            for (String test : a)
                if (!b.contains(test))
                    bad++;
        } else {
            bad = a.size() + b.size();
            for (String test : a)
                if (b.contains(test)) {
                    bad -= 2;
                }
        }
        return bad;
    }

    @Override
    public String toString() {
        return Parent.Backing.getClassName() + "." + Backing.getName() + Backing.getSignature() + (Parent.isRemapped() || isRemapped() ? " (" + Parent.getRemappedName() + "." + getRemappedName() + ")" : "");
    }

    public boolean isRestricted() {
        return _restricted;
    }
}

package equinox.fingerprint;

import equinox.remap.IRemapDatabase;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.util.ClassPath;
import org.apache.bcel.util.SyntheticRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Fingerprint {
    public final SyntheticRepository _repo;
    private final ZipFile _jar;
    private final Map<String, Class> _loaded;

    public final IRemapDatabase Remap;

    private final Set<String> _embeddedClasses;

    private static void Download(String url, File result) {
        FileOutputStream fos = null;
        try {
            URL website = new URL(url);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            result.getParentFile().mkdirs();
            fos = new FileOutputStream(result);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        result.delete();
    }

    public Fingerprint(File path, IRemapDatabase remap) throws IOException {
        File mcjar = new File(path, "minecraft.jar");
        File json = new File(path, "minecraft.json");
        StringBuilder cp = new StringBuilder();
        if (json.exists()) {
            JSONObject jsonData = (JSONObject) JSONValue.parse(new FileReader(json));
            JSONArray libraries = (JSONArray) jsonData.get("libraries");
            for (Object entry : libraries) {
                JSONObject lib = (JSONObject) entry;
                String name = (String) lib.get("name");
                JSONObject artifact = (JSONObject) ((JSONObject) lib.get("downloads")).get("artifact");
                File libPath = new File("lib" + File.separator + artifact.get("path"));
                if (!libPath.exists()) {
                    System.out.println("Download " + name + " from " + artifact.get("url"));
                    Download((String) artifact.get("url"), libPath);
                }
                if (cp.length() > 0)
                    cp.append(File.pathSeparator);
                cp.append(libPath.getAbsolutePath());
            }
            JSONObject minecraftArtifact = (JSONObject) ((JSONObject) jsonData.get("downloads")).get("client");
            if (!mcjar.exists() || mcjar.length() != ((Number) minecraftArtifact.get("size")).longValue()) {
                System.out.println("Download " + minecraftArtifact.get("url"));
                Download((String) minecraftArtifact.get("url"), mcjar);
            }
            if (cp.length() > 0)
                cp.append(File.pathSeparator);
            cp.append(mcjar.getAbsolutePath());
        }
        _jar = new ZipFile(mcjar);
        _repo = SyntheticRepository.getInstance(new ClassPath(cp.toString()));
        _embeddedClasses = new HashSet<>();
        {
            for (Enumeration<? extends ZipEntry> en = _jar.entries(); en.hasMoreElements(); ) {
                ZipEntry entry = en.nextElement();
                String name = entry.getName();
                if (name.endsWith(".class")) {
                    String typeName = name.substring(0, name.length() - 6);
                    _embeddedClasses.add(typeName);
                }
            }
        }

        _loaded = new HashMap<>();
        Remap = remap;
    }

    public Class GetClass(String name) {
        name = name.replace('/', '.');
        if (!_loaded.containsKey(name))
            Load(name);
        return _loaded.get(name);
    }

    public Iterable<Class> Classes() {
        return _loaded.values();
    }

    public void LoadAll() {
        List<String> toLoad = _embeddedClasses.stream().collect(Collectors.toList());
        int isize = _loaded.size();
        for (int i = 0; i < toLoad.size(); i++) {
            GetClass(toLoad.get(i));
            if ((i % 500) == 0)
                System.out.println("Loading classes for " + _jar.getName() + "... " + (_loaded.size() - isize) + " / " + toLoad.size() + " (" + ((_loaded.size() - isize) * 100.0 / toLoad.size()) + "%)");
        }
        for (Class c : _loaded.values())
            c.EnsureMethods();
    }

    public void PrintInheritanceTree() {
        for (Class c : _loaded.values())
            if (c.BaseTypes.size() == 0)
                PrintInheritanceTree(c, "");
    }

    private void PrintInheritanceTree(Class c, String indent) {
        String smartName = null;
        if (Remap != null)
            smartName = Remap.RemappedName(c);
        System.out.println(indent + " - " + (c.Backing.isInterface() ? "[I] " : "") + c.Backing.getClassName() + "\t" + smartName);
        for (Class child : c.DerivedTypes)
            PrintInheritanceTree(child, indent + " ");
    }

    private Class Load(JavaClass type) {
        int nest = type.getClassName().lastIndexOf('$');
        Class fp = new Class(this, type, _embeddedClasses.contains(type.getClassName()));
        if (_loaded.containsKey(type.getClassName())) {
            Class curr = _loaded.get(type.getClassName());
            throw new RuntimeException("Type " + type.getClassName() + " was loaded twice");
        }
        _loaded.put(type.getClassName(), fp);
        fp.EnsureInheritance();
        for (String intf : fp.Backing.getInterfaceNames())
            try {
                if (!fp.Backing.getClassName().equals(intf))
                    GetClass(intf)._implementors.add(fp);
            } catch (RuntimeException e) {
            }
        try {
            if (!fp.Backing.getSuperclassName().equals(fp.Backing.getClassName()))
                GetClass(fp.Backing.getSuperclassName())._implementors.add(fp);
        } catch (RuntimeException e) {
        }

        if (nest != -1) {
            String parentType = type.getClassName().substring(0, nest);
            fp.NestedFor = GetClass(parentType);
            fp.NestedFor._nestedClasses.add(fp);
        }
        return fp;
    }

    public boolean IsMinecraft(String typeName) {
        try {
            Class entry = GetClass(typeName);
            return entry != null && entry.IsMinecraft;
        } catch (Exception e) {
            return false;
        }
    }

    public Collection<Method> MethodsWithConstant(String constant, boolean isStringConstant) {
        List<Method> ms = new ArrayList<>();
        for (Class c : Classes()) {
            if (isStringConstant && !c.HasStringConstant(constant))
                continue;
            for (Method m : c._methods) {
                m.EnsureILAnalysis();
                if (m.HasConstant(constant))
                    ms.add(m);
            }
        }
        return ms;
    }


    public boolean AInheritsFromB(String a, String b) {
        b = b.replace('.', '/');
        Set<String> explored = new HashSet<>();
        List<String> explore = new ArrayList<>();
        explore.add(a);
        while (explore.size() > 0) {
            String tmp = explore.remove(explore.size() - 1);
            if (!explored.add(tmp))
                continue;
            if (tmp.equals(b))
                return true;
            // not part of the minecraft types
            if (tmp.contains("."))
                continue;
            Class test = GetClass(tmp);
            if (test.getRemappedName().equals(b) || test.getObfuscatedName().equals(b))
                return true;
            for (String s : test.BaseTypes)
                explore.add(s.replace('.', '/'));
        }
        return false;
    }

    private Class Load(String typeName) {
        try {
            return Load(_repo.loadClass(typeName));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

package equinox.expanders;

import equinox.fingerprint.Field;
import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class DualExplicitMapper extends AbstractExpander {
    protected abstract Map<String, Object> Compute(Fingerprint fp);

    protected abstract boolean UseGeneratedNames();

    @Override
    public int Expand() {
        Map<String, Object> oldMap = Compute(OldData);
        Map<String, Object> newMap = Compute(NewData);
        if (oldMap.size() == 0 || newMap.size() == 0) {
//            System.out.println("Dual explicit " + getClass().getSimpleName() + " didn't give any results");
            return 0;
        }
        int modified = 0;
        for (Map.Entry<String, Object> oldEntry : oldMap.entrySet()) {
            Object newEntry = newMap.getOrDefault(oldEntry.getKey(), null);
            if (newEntry == null)
                continue;
            if (oldEntry.getValue().getClass() != newEntry.getClass()) {
                System.out.println("Dual explicit " + oldEntry.getKey() + " resolved to different types " + oldEntry.getValue() + " and " + newEntry);
                continue;
            }
            if (newEntry instanceof Class) {
                if (Link((Class) oldEntry.getValue(), (Class) newEntry))
                    modified++;
            } else if (newEntry instanceof Field) {
                if (Link((Field) oldEntry.getValue(), (Field) newEntry))
                    modified++;
            } else if (newEntry instanceof Method) {
                if (LinkMethodGroup((Method) oldEntry.getValue(), (Method) newEntry))
                    modified++;
            } else
                System.out.println("Dual explicit " + oldEntry.getKey() + " resolved to strange type " + newEntry.getClass());
        }
        if (UseGeneratedNames()) {
            for (Map.Entry<String, Object> newEntry : newMap.entrySet()) {
                if (oldMap.containsKey(newEntry.getKey()))
                    continue;
                String[] rawParts = newEntry.getKey().split("#");
                String nname = rawParts[rawParts.length - 1];
                Object entry = newEntry.getValue();
                boolean mod = false;
                if (entry instanceof Class) {
                    mod |= !nname.equals(OverrideEntries.RemappedName((Class) entry));
                    OverrideEntries.Remap((Class) entry, nname);
                } else if (entry instanceof Field) {
                    mod |= !nname.equals(OverrideEntries.RemappedName((Field) entry));
                    OverrideEntries.Remap((Field) entry, nname);
                } else if (entry instanceof Method) {
                    mod |= !nname.equals(OverrideEntries.RemappedName((Method) entry));
                    OverrideEntries.Remap((Method) entry, nname);
                } else {
                    System.out.println("Dual explicit " + newEntry.getKey() + " resolved to strange type " + entry.getClass());
                    continue;
                }
                if (mod) {
//                    System.out.println("Using generated name for " + newEntry.getKey() + " @ " + entry + " -> " + rawParts[rawParts.length - 1]);
                    modified++;
                }
            }
        }
        return modified;
    }
}

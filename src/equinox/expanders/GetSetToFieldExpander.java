package equinox.expanders;

import equinox.fingerprint.Field;
import equinox.fingerprint.Method;
import equinox.util.GetterSetterUtil;
import jdk.nashorn.internal.objects.annotations.Getter;

import java.util.Map;
import java.util.stream.Collectors;

public class GetSetToFieldExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int modified = 0;
        for (Map.Entry<Method, Method> k : LinkEntries.LinkedMethods().collect(Collectors.toList())) {
            GetterSetterUtil newInfo = GetterSetterUtil.GetMethodInfo(k.getKey());
            GetterSetterUtil oldInfo = GetterSetterUtil.GetMethodInfo(k.getValue());
            if (newInfo == null || oldInfo == null)
                continue;
            if (newInfo.Writing != oldInfo.Writing) {
                System.out.println("WARN new " + newInfo + " while old " + oldInfo);
                continue;
            }
            if (Link(oldInfo.AccessedField, newInfo.AccessedField))
                modified++;
        }
        return modified;
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import equinox.util.BooleanTri;
import equinox.util.ExplicitEquality;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.generic.*;

import java.util.Map;
import java.util.stream.Collectors;

public class MethodProxyMethodExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int changed = 0;
        for (Map.Entry<Method, Method> methods : LinkEntries.LinkedMethods().collect(Collectors.toList())) {
            Method nw = methods.getKey();
            Method old = methods.getValue();
            Method bouncedNew = GetSingleInvoke(nw);
            Method bouncedOld = GetSingleInvoke(old);
            if (bouncedNew != null && bouncedOld != null && !bouncedNew.isSynthetic() && !bouncedOld.isSynthetic() &&
                    bouncedNew.Parent.IsMinecraft && bouncedOld.Parent.IsMinecraft &&
                    !bouncedNew.IsUnobfuscated && !bouncedOld.IsUnobfuscated && MethodFuzzyEqual(bouncedOld, bouncedNew)) {
                LinkMethodGroup(bouncedOld, bouncedNew);
            }
        }
        return changed;
    }

    private Method GetSingleInvoke(Method m) {
        Code c = m.Backing.getCode();
        if (c == null)
            return null;
        InstructionList il = new InstructionList(c.getCode());
        ConstantPoolGen cpg = new ConstantPoolGen(c.getConstantPool());
        Instruction[] set = il.getInstructions();
        Method invoked = null;
        for (Instruction i : set) {
            if (i instanceof InvokeInstruction && !(i instanceof INVOKEDYNAMIC)) {
                InvokeInstruction ii = (InvokeInstruction) i;
                Type clazzName = ii.getReferenceType(cpg);
                if (!(clazzName instanceof ObjectType))
                    return null;
                Class clazz = m.Parent.Parent.GetClass(((ObjectType)clazzName).getClassName());
                if (clazz == null)
                    return null;
                Method bounced = clazz.GetMethod(ii.getMethodName(cpg), ii.getSignature(cpg));
                if (bounced == null)
                    return null;
                if (invoked != null && invoked != bounced)
                    return null;
                invoked = bounced;
            }
        }
        return invoked;
    }


    public boolean MethodFuzzyEqual(Method self, Method other) {
        if (self == other)
            return true;
        if (self.Backing.getAccessFlags() != other.Backing.getAccessFlags())
            return false;
        Fingerprint srcSelf = self.Parent.Parent;
        Fingerprint srcOther = other.Parent.Parent;

        Type[] myTypes = self.Backing.getArgumentTypes();
        Type[] otherTypes = other.Backing.getArgumentTypes();
        if (myTypes.length != otherTypes.length)
            return false;

        boolean selfHasCode = self.Backing.getCode() != null;
        boolean otherHasCode = other.Backing.getCode() != null;
        if (selfHasCode != otherHasCode)
            return false;
        for (int i = 0; i < Math.min(myTypes.length, otherTypes.length); i++) {
            BooleanTri res = ExplicitEquality.TypesEqual(srcSelf, myTypes[i], srcOther, otherTypes[i]);
            if (res == BooleanTri.FALSE)
                return false;
        }
        {
            BooleanTri res = ExplicitEquality.TypesEqual(srcSelf, self.Backing.getReturnType(), srcOther, other.Backing.getReturnType());
            if (res == BooleanTri.FALSE)
                return false;
        }
        return true;
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Method;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.generic.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ConstantReturnMethodExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int changed = 0;
        for (Map.Entry<Class, Class> linked : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Map<Object, Method> nw = GetConstantMethods(linked.getKey());
            Map<Object, Method> old = GetConstantMethods(linked.getValue());
            for (Map.Entry<Object, Method> e : nw.entrySet()) {
                Method oldEntry = old.get(e.getKey());
                if (oldEntry != null && LinkMethodGroup(oldEntry, e.getValue()))
                    changed++;
            }
        }
        return changed;
    }

    private Map<Object, Method> GetConstantMethods(Class clz) {
        Map<Object, Set<Method>> results = new HashMap<>();
        for (Method m : clz.Methods()) {
            Code c = m.Backing.getCode();
            if (c == null)
                continue;
            InstructionList il = new InstructionList(c.getCode());
            ConstantPoolGen cpg = new ConstantPoolGen(c.getConstantPool());
            Instruction[] set = il.getInstructions();
            if (set.length != 2)
                continue;
            if (!(set[1] instanceof RET))
                continue;
            if (!(set[0] instanceof LDC))
                continue;
            Object val = ((LDC) set[0]).getValue(cpg);
            Set<Method> rs = results.getOrDefault(val, null);
            if (rs == null)
                results.put(val, rs = new HashSet<>());
            rs.add(m);
        }
        Map<Object, Method> fin = new HashMap<>();
        for (Map.Entry<Object, Set<Method>> s : results.entrySet())
            if (s.getValue().size() == 1)
                fin.put(s.getKey(), s.getValue().iterator().next());
        return fin;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

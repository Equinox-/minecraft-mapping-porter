package equinox.expanders;

import equinox.fingerprint.Class;

import java.util.Map;
import java.util.stream.Collectors;

public class NestedForTypeExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int changed = 0;
        for (Map.Entry<Class, Class> pair : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Class a = pair.getKey();
            Class b = pair.getValue();
            if (a.NestedFor != null && b.NestedFor != null && Link(b.NestedFor, a.NestedFor))
                changed++;
        }
        return changed;
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

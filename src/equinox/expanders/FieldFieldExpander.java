package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.util.ExplicitEquality;
import equinox.util.GenericAwareType;
import org.apache.bcel.generic.Type;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FieldFieldExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int modified = 0;
        for (Map.Entry<Class, Class> pair : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Map<GenericAwareType, List<Field>> typesForNew = FieldsByType(pair.getKey(), x -> x.getRemappedName(), null);
            Set<Field> skipped = new HashSet<>();
            for (List<Field> f : typesForNew.values())
                for (Iterator<Field> itr = f.iterator(); itr.hasNext(); ) {
                    Field opposite = LinkEntries.Linked(itr.next());
                    if (opposite != null) {
                        skipped.add(opposite);
                        itr.remove();
                    }
                }

            Map<GenericAwareType, List<Field>> typesForOld = FieldsByType(pair.getValue(), x -> x.getRemappedName(), x -> !skipped.contains(x));
            for (Map.Entry<GenericAwareType, List<Field>> t : typesForNew.entrySet()) {
                if (t.getValue().size() != 1)
                    continue;
                List<Field> other = typesForOld.getOrDefault(t.getKey(), null);

                if (other == null || other.size() != 1)
                    continue;
                if (Link(other.get(0), t.getValue().get(0)))
                    modified++;
            }
        }
        return modified;
    }

    private Map<GenericAwareType, List<Field>> FieldsByType(Class c, Function<Class, String> mapper, Predicate<Field> include) {
        Map<GenericAwareType, List<Field>> res = new HashMap<>();
        for (Field f : c.Fields())
            if (include == null || include.test(f)) {
                GenericAwareType t = f.Type;
                t = ExplicitEquality.RemapType(c.Parent, t, mapper);
                if (t == null)
                    continue;
                List<Field> e = res.getOrDefault(t, null);
                if (e == null)
                    res.put(t, e = new ArrayList<>());
                e.add(f);
            }
        return res;
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

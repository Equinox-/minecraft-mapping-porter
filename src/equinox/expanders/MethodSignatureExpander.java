package equinox.expanders;

import equinox.fingerprint.Method;

import java.util.stream.Collectors;

public class MethodSignatureExpander extends BcelTypeMapper {
    @Override
    public int Expand() {
        _modified = 0;
        for (Method newMethod : LinkEntries.RemappedMethods().collect(Collectors.toList())) {
            Method old = LinkEntries.Linked(newMethod);

            Link(old.Parent, newMethod.Parent);

            boolean failed = false;
            failed |= !MapType(old.Backing.getReturnType(), newMethod.Backing.getReturnType());
            for (int i = 0; i < Math.min(old.Backing.getArgumentTypes().length, newMethod.Backing.getArgumentTypes().length); i++)
                failed |= !MapType(old.Backing.getArgumentTypes()[i], newMethod.Backing.getArgumentTypes()[i]);
            if (failed) {
                System.out.println("Failed to apply method signature expander on " + newMethod + " -> " + old + ", mojang likely changed it");
            }
        }
        return _modified;
    }
    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

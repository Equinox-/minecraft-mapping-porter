package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.generic.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class RendererToModelExpander extends DualExplicitMapper {
    private static final String _typeRender = "net/minecraft/client/renderer/entity/Render";
    private static final String _typeModel = "net/minecraft/client/renderer/entity/model/ModelBase";

    @Override
    protected Map<String, Object> Compute(Fingerprint fp) {
        Map<String, HashSet<Class>> build = new HashMap<>();
        for (Class c : fp.Classes()) {
            if (!c.isRemapped() || !fp.AInheritsFromB(c.getObfuscatedName(), _typeRender))
                continue;
            methods:
            for (Method m : c.Methods()) {
                if (!m.isCtor())
                    continue;

                Code code = m.Backing.getCode();
                if (code == null)
                    continue;
                InstructionList il = new InstructionList(code.getCode());
                ConstantPoolGen cpg = new ConstantPoolGen(code.getConstantPool());
                Instruction[] set = il.getInstructions();
                String modelType = null;
                for (int id = 0; id < il.size(); id++) {
                    Instruction i = set[id];
                    if (i instanceof NEW) {
                        NEW nw = (NEW) i;
                        String type = nw.getLoadClassType(cpg).getClassName();
                        if (fp.AInheritsFromB(type, _typeModel)) {
                            if (modelType != null)
                                continue methods;
                            modelType = type;
                        }
                    } else if (i instanceof INVOKESPECIAL) {
                        INVOKESPECIAL call = (INVOKESPECIAL) i;
                        if (call.getClassName(cpg).equals(c.Backing.getSuperclassName())) {
                            if (modelType != null) {
                                Class modelClass = fp.GetClass(modelType);
                                if (modelClass != null) {
                                    String key = c.getRemappedName();
                                    HashSet<Class> rest = build.getOrDefault(key, null);
                                    if (rest == null)
                                        build.put(key, rest = new HashSet<>());
                                    rest.add(modelClass);
                                }
                            }
                            continue methods;
                        }
                    }
                }
            }
        }
        Map<String, Object> results = new HashMap<>();
        for (Map.Entry<String, HashSet<Class>> e : build.entrySet())
            if (e.getValue().size() == 1)
                results.put(e.getKey(), e.getValue().iterator().next());
        return results;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return false;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

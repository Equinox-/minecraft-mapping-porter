package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Method;
import equinox.util.MultiMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class NestedToExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int changed = 0;
        for (Map.Entry<Class, Class> ent : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Class nw = ent.getKey();
            Class old = ent.getValue();

            Map<String, List<Class>> nwNested = GroupedNestedBySuperTypes(nw);
            Map<String, List<Class>> oldNested = GroupedNestedBySuperTypes(old);
            for (Map.Entry<String, List<Class>> e : nwNested.entrySet()) {
                if (e.getValue().size() != 1)
                    continue;
                String key = e.getKey();
                List<Class> oldNests = oldNested.get(key);
                if (oldNests == null || oldNests.size() != 1)
                    continue;
                Class nwNest = e.getValue().get(0);
                Class oldNest = oldNests.get(0);
                if (Link(oldNest, nwNest)) {
                    changed++;
                }
            }
        }
        return changed;
    }

    private Map<String, List<Class>> GroupedNestedBySuperTypes(Class obj) {
        Map<String, List<Class>> res = new HashMap<>();
        for (Class nest : obj.NestedTypes()) {
            for (String sup : nest.BaseTypes) {
                if (sup.contains("/") || sup.contains("."))
                    MultiMap.AddMultiList(res, sup, nest);
                else {
                    try {
                        Class clazz = obj.Parent.GetClass(sup);
                        if (clazz.isRemapped())
                            MultiMap.AddMultiList(res, clazz.getRemappedName(), nest);
                    } catch (Exception e) {
                    }
                }
            }
        }
        return res;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

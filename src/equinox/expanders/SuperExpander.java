package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;
import equinox.remap.FileBackedRemap;
import org.apache.bcel.classfile.JavaClass;

import java.util.*;
import java.util.stream.Collectors;

public class SuperExpander extends AbstractExpander {
    @Override
    public int Expand() {
        // newObf -> (oldObf -> newClassesCausing)
        Map<Class, Map<Class, Set<Class>>> remapCausingMap = new HashMap<>();

        for (Map.Entry<Class, Class> remapped : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Class newClazz = remapped.getKey();
            Class oldClazz = remapped.getValue();

            String newSuperName = newClazz.Backing.getSuperclassName();
            if (!NewData.IsMinecraft(newSuperName))
                continue;
            Class newSuper = NewData.GetClass(newSuperName);

            String oldSuperName = oldClazz.Backing.getSuperclassName();
            if (!OldData.IsMinecraft(oldSuperName))
                continue;
            Class oldSuper = OldData.GetClass(oldSuperName);

            Map<Class, Set<Class>> renamesForSuper = remapCausingMap.getOrDefault(newSuper, null);
            if (renamesForSuper == null)
                remapCausingMap.put(newSuper, renamesForSuper = new HashMap<>());
            Set<Class> causersForDeobf = renamesForSuper.getOrDefault(oldSuper, null);
            if (causersForDeobf == null)
                renamesForSuper.put(oldSuper, causersForDeobf = new HashSet<>());
            causersForDeobf.add(newClazz);
        }

        // Remove all superclasses from the resolve tree - they have to be resolved after self.
        for (Map.Entry<Class, Map<Class, Set<Class>>> entry : remapCausingMap.entrySet().stream().collect(Collectors.toList())) {
            if (RemapsTo(entry.getKey(), entry.getValue()) == null) {
                remapCausingMap.remove(entry.getKey());
                continue;
            }
            Class key = entry.getKey();
            while (key != null) {
                if (!NewData.IsMinecraft(key.Backing.getSuperclassName()))
                    break;
                key = NewData.GetClass(key.Backing.getSuperclassName());
                remapCausingMap.remove(key);
            }
        }

        int modified = 0;
        for (Map.Entry<Class, Map<Class, Set<Class>>> expando : remapCausingMap.entrySet()) {
            Class target = RemapsTo(expando.getKey(), expando.getValue());
            if (target == null)
                continue;
            if (Link(target, expando.getKey())) {
//                DumpMapInfo(expando.getKey(), expando.getValue());
                modified++;
            }
        }
        return modified;
    }

    private static final Set<Class> _badClasses = new HashSet<>();

    private static void DumpMapInfo(Class newSuperclass, Map<Class, Set<Class>> oldSuperclasses) {
        System.out.println("SuperClass: " + newSuperclass);
        for (Map.Entry<Class, Set<Class>> e : oldSuperclasses.entrySet())
            System.out.println("\t- old canidate " + e.getKey() + " caused by " + e.getValue().stream().map(x -> x.toString()).reduce("", (a, b) -> a + ", " + b));
    }

    private static Class RemapsTo(Class newSuperclass, Map<Class, Set<Class>> oldSuperclasses) {
        if (oldSuperclasses.size() == 1)
            return oldSuperclasses.keySet().iterator().next();
        List<Map.Entry<Class, Set<Class>>> results = oldSuperclasses.entrySet().stream().sorted(Comparator.comparingInt(a -> -a.getValue().size())).collect(Collectors.toList());
        int total = results.stream().mapToInt(x -> x.getValue().size()).sum();
        if (results.get(0).getValue().size() < results.get(1).getValue().size() * 2) {
            if (_badClasses.add(newSuperclass)) {
                System.out.println("Failed to remap");
                DumpMapInfo(newSuperclass, oldSuperclasses);
            }
            return null;
        }
        return results.get(0).getKey();
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

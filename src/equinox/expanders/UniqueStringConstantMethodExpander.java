package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UniqueStringConstantMethodExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int changed = 0;
        for (Map.Entry<Class, Class> entry : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Class nw = entry.getKey();
            Map<String, List<Method>> tableNew = buildUniqueMethodTable(nw);
            Class old = entry.getValue();
            Map<String, List<Method>> tableOld = buildUniqueMethodTable(old);
            for (Map.Entry<String, List<Method>> nwEntry : tableNew.entrySet()) {
                if (nwEntry.getValue().size() != 1)
                    continue;
                String key = nwEntry.getKey();
                List<Method> oldMethods = tableOld.get(key);
                if (oldMethods == null || oldMethods.size() != 1)
                    continue;
                Method nwMethod = nwEntry.getValue().get(0);
                Method oldMethod = oldMethods.get(0);
                SingleMethodMapper.Confidence result = SingleMethodMapper.MethodFuzzyEqual(nwMethod, oldMethod);
                if (result != null&&LinkMethodGroup(oldMethod, nwMethod))
                    changed++;
            }
        }
        return changed;
    }

    private Map<String, List<Method>> buildUniqueMethodTable(Class c) {
        Map<String, List<Method>> res = new HashMap<>();
        for (Method m : c.Methods())
            for (String s : m.StringConstants()) {
                List<Method> t = res.get(s);
                if (t == null)
                    res.put(s, t = new ArrayList<>());
                t.add(m);
            }
        return res;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

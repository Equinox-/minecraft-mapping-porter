package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Method;
import equinox.util.GetterSetterUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FieldToGetSetExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int modified = 0;
        Map<Class, List<GetterSetterUtil>> getterSetters = new HashMap<>();
        for (Map.Entry<Field, Field> k : LinkEntries.LinkedFields().collect(Collectors.toList())) {
            List<GetterSetterUtil> newList = getterSetters.getOrDefault(k.getKey().Parent, null);
            if (newList == null)
                getterSetters.put(k.getKey().Parent, newList = GetterSetters(k.getKey().Parent));
            List<GetterSetterUtil> oldList = getterSetters.getOrDefault(k.getValue().Parent, null);
            if (oldList == null)
                getterSetters.put(k.getValue().Parent, oldList = GetterSetters(k.getValue().Parent));

            for (int info = 0; info <= 1; info++) {
                boolean writeMode = info != 0;

                GetterSetterUtil[] newRel = newList.stream().filter(x -> x.AccessedField == k.getKey() && x.Writing == writeMode).toArray(x -> new GetterSetterUtil[x]);
                GetterSetterUtil[] oldRel = oldList.stream().filter(x -> x.AccessedField == k.getValue() && x.Writing == writeMode).toArray(x -> new GetterSetterUtil[x]);
                if (newRel.length != 1 || oldRel.length != 1)
                    continue;

                Method newMethod = newRel[0].Accessor;
                Method oldMethod = oldRel[0].Accessor;

                if (LinkMethodGroup(oldMethod, newMethod))
                    modified++;
            }
        }
        return modified;
    }

    private List<GetterSetterUtil> GetterSetters(Class c) {
        List<GetterSetterUtil> res = new ArrayList<>();
        for (Method m : c.Methods()) {
            if (m.IsUnobfuscated || m.Group().stream().anyMatch(x -> x.Parent.IsUnobfuscated))
                continue;
            GetterSetterUtil info = GetterSetterUtil.GetMethodInfo(m);
            if (info != null)
                res.add(info);
        }
        return res;
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

package equinox.expanders;

import equinox.fingerprint.Field;
import equinox.util.BooleanTri;
import equinox.util.ExplicitEquality;
import org.apache.bcel.generic.Type;

import java.util.Map;
import java.util.stream.Collectors;

public class FieldTypeExpander extends BcelTypeMapper {
    @Override
    public int Expand() {
        _modified = 0;
        for (Map.Entry<Field, Field> fields : LinkEntries.LinkedFields().collect(Collectors.toList())) {
            Field newField = fields.getKey();
            Type newType = newField.Backing.getType();
            Field oldField = fields.getValue();
            Type oldType = oldField.Backing.getType();
            if (ExplicitEquality.TypesEqual(oldField.Parent.Parent, oldType, newField.Parent.Parent, newType) == BooleanTri.FALSE)
                throw new RuntimeException("Field link " + newField + " -> " + oldField + " has mismatched types "
                        + newType + " (" + ExplicitEquality.RemapType(newField.Parent.Parent, newType) + ")" + " new <-> old "
                        + oldType + " (" + ExplicitEquality.RemapType(oldField.Parent.Parent, oldType) + ")");
            MapType(oldType, newType);
        }
        return _modified;
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

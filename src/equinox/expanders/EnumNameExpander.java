package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.generic.*;

import java.util.HashMap;
import java.util.Map;

public class EnumNameExpander extends DualExplicitMapper {
    @Override
    protected Map<String, Object> Compute(Fingerprint fp) {
        Map<String, Object> enumFields = new HashMap<>();
        for (Class c : fp.Classes()) {
            if (!c.isRemapped())
                continue;
            if (!fp.AInheritsFromB(c.getObfuscatedName(), "java.lang.Enum"))
                continue;

            for (Method m : c.Methods()) {
                if (!m.isStaticCtor())
                    continue;
                Code code = m.Backing.getCode();
                InstructionList il = new InstructionList(code.getCode());
                ConstantPoolGen cpg = new ConstantPoolGen(code.getConstantPool());
                Instruction[] set = il.getInstructions();
                String activeStringKey = null;
                boolean sawCtorCall = false;
                for (int id = 0; id < il.size(); id++) {
                    Instruction i = set[id];
                    if (i instanceof LDC) {
                        LDC ldc = (LDC) i;
                        Object val = ldc.getValue(cpg);
                        if (val instanceof String)
                            activeStringKey = (String) val;
                    } else if (i instanceof PUTSTATIC && id > 0 && set[id - 1] instanceof INVOKESPECIAL && activeStringKey != null) {
                        PUTSTATIC put = (PUTSTATIC) i;
                        INVOKESPECIAL call = (INVOKESPECIAL) set[id - 1];
                        Type container = put.getFieldType(cpg);
                        if (fp.AInheritsFromB(call.getClassName(cpg), c.getObfuscatedName())
                                && container instanceof ObjectType &&
                                ((ObjectType) container).getClassName().equals(c.getObfuscatedName())) {
                            Field f = c.GetField(put.getFieldName(cpg));
                            if (f != null)
                                enumFields.put(c.getRemappedName() + "_" + activeStringKey, f);
                        }
                        activeStringKey = null;
                    }
                }
            }
        }
        return enumFields;
    }

    @Override
    protected boolean UseGeneratedNames() {
        return false;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

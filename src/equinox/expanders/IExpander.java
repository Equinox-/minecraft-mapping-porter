package equinox.expanders;

import equinox.fingerprint.Fingerprint;
import equinox.remap.CustomRemap;
import equinox.remap.LinkedRemap;

public interface IExpander {
    void Init(Fingerprint old, Fingerprint nw, CustomRemap overrides, LinkedRemap linkages);

    int Expand();

    boolean RepeatUntilStable();
}

package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import equinox.remap.CustomRemap;
import equinox.remap.LinkedRemap;

import java.util.Set;

public abstract class AbstractExpander implements IExpander {
    protected Fingerprint NewData;
    protected Fingerprint OldData;
    protected CustomRemap OverrideEntries;
    protected LinkedRemap LinkEntries;

    public abstract boolean RepeatUntilStable();

    @Override
    public void Init(Fingerprint old, Fingerprint nw, CustomRemap overrides, LinkedRemap linkages) {
        this.OldData = old;
        this.NewData = nw;
        this.OverrideEntries = overrides;
        this.LinkEntries = linkages;
    }

    protected boolean Link(Class old, Class nw) {
        assert old.Parent == OldData : "Old wasn't part of old fingerprint";
        assert nw.Parent == NewData : "New wasn't part of new fingerprint";
        if (old==nw)
            throw new IllegalArgumentException("Old == new " + old);
        if (LinkEntries.Link(nw, old)) {
            OverrideEntries.Remap(nw, null);
            return true;
        }
        return false;
    }

    protected boolean LinkMethodGroup(Method old, Method nw) {
        assert old.Parent.Parent == OldData : "Old wasn't part of old fingerprint";
        assert nw.Parent.Parent == NewData : "New wasn't part of new fingerprint";
//        assert old.isRemapped() : "Old method wasn't remapped";
        if (!old.isRemapped())
            return false;
        String oldDeob = old.getRemappedName();

        boolean mod = false;
        mod |= Link(old.Parent, nw.Parent);

        Set<Method> oldGroup = old.Group();

        // Remap the group:
        for (Method m : nw.Group()) {
            if (!m.Parent.IsMinecraft)
                continue;
            // Can we do a full link?
            Class otherClass = LinkEntries.Linked(m.Parent);
            if (otherClass != null) {
                Method otherMethod = oldGroup.stream().filter(x -> x.Parent == otherClass).findAny().orElse(null);
                if (otherMethod != null && otherMethod.Parent.IsMinecraft) {
                    if (LinkEntries.Link(m, otherMethod)) {
                        OverrideEntries.Remap(m, null);
                        mod = true;
                    }
                    continue;
                }
//                System.out.println("Failed to find matching method for " + old + " on " + otherClass);
            }
            String existing = m.getRemappedName();
            if (m.isRemapped() && !oldDeob.equals(existing))
                throw new RuntimeException("Produced two deob names " + existing + " -> " + oldDeob + " when remapping group for " + nw);
            if (!oldDeob.equals(m.Backing.getName()))
                OverrideEntries.Remap(m, oldDeob);
            mod |= existing == null;
        }
        return mod;
    }

    protected boolean Link(Field old, Field nw) {
        assert old.Parent.Parent == OldData : "Old wasn't part of old fingerprint";
        assert nw.Parent.Parent == NewData : "New wasn't part of new fingerprint";
        boolean mod = false;
        mod |= Link(old.Parent, nw.Parent);
        if (LinkEntries.Link(nw, old)) {
            OverrideEntries.Remap(nw, null);
            return mod;
        }
        return mod;
    }
}

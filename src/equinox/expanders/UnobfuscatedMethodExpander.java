package equinox.expanders;

import equinox.fingerprint.Class;
import equinox.fingerprint.Method;

import java.util.Map;
import java.util.stream.Collectors;

public class UnobfuscatedMethodExpander extends AbstractExpander {
    @Override
    public int Expand() {
        int changed = 0;
        for (Map.Entry<Class, Class> ent : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Class nw = ent.getKey();
            Class old = ent.getValue();
            for (Method newMethod : nw.Methods()) {
                for (Method oldMethod : old.Methods()) {
                    if (!newMethod.Backing.getName().equals(oldMethod.Backing.getName()))
                        continue;
                    if (SingleMethodMapper.MethodFuzzyEqual(newMethod, oldMethod) != null) {
                        Method newRoot = newMethod.GroupRoot();
                        Method oldRoot = oldMethod.GroupRoot();
                        if (!newRoot.Parent.IsMinecraft && !oldRoot.Parent.IsMinecraft && newRoot.Backing.getSignature().equals(oldRoot.Backing.getSignature())) {
                            if (LinkMethodGroup(oldMethod, newMethod))
                                changed++;
                            break;
                        }
                    }
                }
            }
        }
        return changed;
    }

    @Override
    public boolean RepeatUntilStable() {
        return false;
    }
}

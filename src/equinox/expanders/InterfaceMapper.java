package equinox.expanders;

import equinox.fingerprint.Class;

import java.util.*;
import java.util.stream.Collectors;

public class InterfaceMapper extends AbstractExpander {
    @Override
    public int Expand() {
        int changed = 0;
        for (Map.Entry<Class, Class> ent : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Class nw = ent.getKey();
            Class old = ent.getValue();
            Set<Class> nwInterfaces = FilteredBaseTypes(nw);
            Set<Class> oldInterfaces = FilteredBaseTypes(old);
            for (Class nwInt : nwInterfaces.stream().collect(Collectors.toList())) {
                Class oldInt = LinkEntries.Linked(nwInt);
                if (oldInt != null) {
                    nwInterfaces.remove(nwInt);
                    oldInterfaces.remove(oldInt);
                }
            }
            if (nwInterfaces.size() == 1 && oldInterfaces.size() == 1 && Link(oldInterfaces.iterator().next(), nwInterfaces.iterator().next()))
                changed++;
        }
        return changed;
    }

    private static Set<Class> FilteredBaseTypes(Class s) {
        Set<Class> sp = new HashSet<>();
        for (String base : s.BaseTypes) {
            if (base.contains(".") || base.contains("/"))
                continue;
            sp.add(s.Parent.GetClass(base));
        }
        return sp;
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

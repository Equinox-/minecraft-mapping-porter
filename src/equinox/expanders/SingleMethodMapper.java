package equinox.expanders;

import equinox.fingerprint.*;
import equinox.fingerprint.Class;
import equinox.util.BooleanTri;
import equinox.util.ExplicitEquality;
import org.apache.bcel.generic.Type;

import java.util.*;
import java.util.stream.Collectors;

public class SingleMethodMapper extends AbstractExpander {
    @Override
    public int Expand() {
        int modified = 0;
        Set<Method> visitedMethods = new HashSet<>();
        List<Set<Method>> methodGroups = new ArrayList<>();
        for (Map.Entry<Class, Class> entry : LinkEntries.LinkedClasses().collect(Collectors.toList())) {
            Class newClazz = entry.getKey();
            Class oldClazz = entry.getValue();

            for (Method newMethod : newClazz._methods) {
                if (visitedMethods.contains(newMethod))
                    continue;
                Set<Method> group = newMethod.Group();
                for (Method m : group)
                    if (!visitedMethods.add(m)) {
                        System.out.println("Method appears in two groups? " + m);
                        for (Set<Method> set : methodGroups)
                            if (set.contains(m))
                                System.out.println("\t- " + set.stream().map(x -> x.toString()).sorted().collect(Collectors.toList()));
                        System.out.println("\t- " + group);
                    }
                methodGroups.add(group);
            }
        }

        for (Set<Method> newGroup : methodGroups) {
            Map<String, Map<Method, Method>> mapCausers = new HashMap<>();
            int totalMatches = 0;
            for (Method newMethod : newGroup) {
                Class oldClazz = LinkEntries.Linked(newMethod.Parent);
                if (oldClazz == null)
                    continue;
                Class newClazz = newMethod.Parent;

                Method[] oldMatches = oldClazz._methods.stream().filter(x -> Confidence.All(MethodFuzzyEqual(x, newMethod))).toArray(x -> new Method[x]);
                Method[] newMatches = newClazz._methods.stream().filter(x -> Confidence.All(MethodFuzzyEqual(x, newMethod))).toArray(x -> new Method[x]);
                if (oldMatches.length != 1 || newMatches.length != 1) {
                    oldMatches = oldClazz._methods.stream().filter(x -> Confidence.Any(MethodFuzzyEqual(x, newMethod))).toArray(x -> new Method[x]);
                    newMatches = newClazz._methods.stream().filter(x -> Confidence.Any(MethodFuzzyEqual(x, newMethod))).toArray(x -> new Method[x]);
                    if (oldMatches.length != 1 || newMatches.length != 1)
                        continue;
                }

                Method oldMethod = oldMatches[0];
                String deobfuscatedName = oldMethod.getRemappedName();
                if (deobfuscatedName == null)
                    continue;
                Map<Method, Method> causers = mapCausers.getOrDefault(deobfuscatedName, null);
                if (causers == null)
                    mapCausers.put(deobfuscatedName, causers = new HashMap<>());
                causers.put(newMethod, oldMethod);
                totalMatches++;
            }
            if (mapCausers.size() == 0)
                continue;
            if (totalMatches * 2 < newGroup.size())
                continue;
            if (mapCausers.size() > 1) {
//                System.out.println("Multiple remap targets for " + newGroup.size() + "*" + newGroup + ".  Not remapping anything.");
//                for (Map.Entry<String, Map<Method, Method>> k : mapCausers.entrySet())
//                    System.out.println("\t- " + k.getKey() + " -> " + k.getValue().size() + " " + k.getValue());
                continue;
            }
            Map.Entry<String, Map<Method, Method>> appliedMap = mapCausers.entrySet().iterator().next();
            Method selfRoot = appliedMap.getValue().keySet().iterator().next();
            Method oldAssoc = appliedMap.getValue().get(selfRoot);
            if (LinkMethodGroup(oldAssoc, selfRoot))
                modified++;
        }
        return modified;
    }

    public static class Confidence {
        public final int Matches;
        public final int Tests;

        public Confidence(int match, int tests) {
            Matches = match;
            Tests = tests;
        }

        public static boolean Any(Confidence c) {
            return c != null && c.Matches > 0;
        }

        public static boolean All(Confidence c) {
            return c != null && c.Matches >= c.Tests;
        }
    }

    public static Confidence MethodFuzzyEqual(Method self, Method other) {
        if (self == other)
            return null;
        if (self.Backing.getAccessFlags() != other.Backing.getAccessFlags())
            return null;
        Fingerprint srcSelf = self.Parent.Parent;
        Fingerprint srcOther = other.Parent.Parent;

        Type[] myTypes = self.Backing.getArgumentTypes();
        Type[] otherTypes = other.Backing.getArgumentTypes();
        if (myTypes.length != otherTypes.length)
            return null;

        boolean selfHasCode = self.Backing.getCode() != null;
        boolean otherHasCode = other.Backing.getCode() != null;
        if (selfHasCode != otherHasCode)
            return null;
        if (selfHasCode) {
            int scs = self.GetCodeSize();
            int ocs = other.GetCodeSize();
            int avgCodeSize = Math.max(512, (scs + ocs) / 2);
            if (Math.abs(scs - ocs) > avgCodeSize / 2)
                return null;
        }

        int tests = 0;
        int matches = 0;
        for (int i = 0; i < Math.min(myTypes.length, otherTypes.length); i++) {
            BooleanTri res = ExplicitEquality.TypesEqual(srcSelf, myTypes[i], srcOther, otherTypes[i]);
            tests++;
            if (res == BooleanTri.FALSE)
                return null;
            else if (res == BooleanTri.TRUE)
                matches++;
        }

        {
            BooleanTri res = ExplicitEquality.TypesEqual(srcSelf, self.Backing.getReturnType(), srcOther, other.Backing.getReturnType());
            tests++;
            if (res == BooleanTri.FALSE)
                return null;
            else if (res == BooleanTri.TRUE)
                matches++;
        }

        return new Confidence(matches, tests);
    }

    @Override
    public boolean RepeatUntilStable() {
        return true;
    }
}

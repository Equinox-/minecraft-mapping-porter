package equinox.expanders;

import equinox.fingerprint.Class;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;

public abstract class BcelTypeMapper extends AbstractExpander {

    protected int _modified = 0;

    protected boolean MapType(Type oldType, Type newType) {
        if (oldType.getClass() != newType.getClass())
            return false;
        if (oldType instanceof ArrayType) {
            ArrayType at = (ArrayType) oldType;
            ArrayType bt = (ArrayType) newType;
            if (at.getDimensions() != bt.getDimensions())
                return false;
            return MapType(at.getElementType(), bt.getElementType());
        }
        if (oldType instanceof ObjectType) {
            ObjectType att = (ObjectType) oldType;
            ObjectType btt = (ObjectType) newType;
            String at = att.getClassName();
            String bt = btt.getClassName();
            boolean am = OldData.IsMinecraft(at);
            boolean bm = NewData.IsMinecraft(bt);
            if (am != bm)
                return false;
            if (!am)
                return at.equals(bt);
            Class oldHandle = OldData.GetClass(at);
            Class newHandle = NewData.GetClass(bt);
            if (Link(oldHandle, newHandle))
                _modified++;
            return true;
        }
        return true;
    }
}

package equinox.util;

import equinox.fingerprint.Class;
import equinox.fingerprint.Fingerprint;
import equinox.fingerprint.Method;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.BasicType;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;

import java.util.function.Function;

public class ExplicitEquality {
    public static Type RemapType(Fingerprint src, Type type) {
        return RemapType(src, type, x -> x.getRemappedName());
    }

    public static GenericAwareType RemapType(Fingerprint src, GenericAwareType type, Function<Class, String> selector) {
        GenericAwareType[] args = null;
        if (type.GenericArgs != null) {
            args = new GenericAwareType[type.GenericArgs.length];
            for (int i = 0; i < args.length; i++)
                if (type.GenericArgs[i] != null)
                    args[i] = RemapType(src, type.GenericArgs[i], selector);
        }
        String genType = null;
        if (!isGeneric(type.GenericType)) {
            Type mapped = RemapType(src, Type.getType(type.GenericType), selector);
            genType = mapped != null ? mapped.getSignature() : null;
        } else
            genType = type.GenericType;
        GenericAwareType gat = new GenericAwareType(type.NestedFor != null ? RemapType(src, type.NestedFor, selector) : null, genType, args);
        if (type.NestedFor != null && gat.NestedFor == null)
            return null;
        if (gat.GenericType == null)
            return null;
        if (gat.GenericArgs != null)
            for (int i = 0; i < gat.GenericArgs.length; i++)
                if (gat.GenericArgs[i] == null && type.GenericArgs[i] != null)
                    return null;
        return gat;
    }

    private static boolean isGeneric(String sig) {
        for (int i = 0; i < sig.length(); i++) {
            char c = sig.charAt(i);
            if (c == '[')
                continue;
            return c == 'T';
        }
        return false;
    }

    public static Type RemapType(Fingerprint src, Type type, Function<Class, String> selector) {
        if (type instanceof ArrayType) {
            Type mapped = RemapType(src, ((ArrayType) type).getElementType(), selector);
            if (mapped == null)
                return null;
            return new ArrayType(mapped, ((ArrayType) type).getDimensions());
        }
        if (type instanceof ObjectType) {
            String clazz = ((ObjectType) type).getClassName();
            if (selector != null && src.IsMinecraft(clazz)) {
                Class c = src.GetClass(clazz);
                String path = selector.apply(c);
                if (path == null)
                    return null;
                return new ObjectType(path);
            }
        }
        return type;
    }

    public static BooleanTri TypesEqual(Fingerprint aSrc, String at, Fingerprint bSrc, String bt) {
        boolean atm = aSrc.IsMinecraft(at) && !at.contains("/");
        boolean btm = bSrc.IsMinecraft(bt) && !at.contains("/");
        if (atm != btm)
            return BooleanTri.FALSE;
        if (!atm)
            return at.equals(bt) ? BooleanTri.TRUE : BooleanTri.FALSE;
        if (aSrc.Remap.IsClassRemapped(at) && bSrc.Remap.IsClassRemapped(bt)) {
            String atMapped = aSrc.Remap.RemappedClassName(at);
            String btMapped = bSrc.Remap.RemappedClassName(bt);
            return atMapped.equals(btMapped) ? BooleanTri.TRUE : BooleanTri.FALSE;
        }
        return BooleanTri.MAYBE;
    }

    public static BooleanTri TypesEqual(Fingerprint aSrc, Type aType, Fingerprint bSrc, Type bType) {
        if (aType.getClass() != bType.getClass())
            return BooleanTri.FALSE;
        if (aType instanceof ArrayType) {
            ArrayType at = (ArrayType) aType;
            ArrayType bt = (ArrayType) bType;
            if (at.getDimensions() != bt.getDimensions())
                return BooleanTri.FALSE;
            return TypesEqual(aSrc, at.getElementType(), bSrc, bt.getElementType());
        }
        if (aType instanceof ObjectType) {
            ObjectType att = (ObjectType) aType;
            ObjectType btt = (ObjectType) bType;
            String at = att.getClassName();
            String bt = btt.getClassName();
            return TypesEqual(aSrc, at, bSrc, bt);
        }
        if (aType instanceof BasicType) {
            BasicType at = (BasicType) aType;
            BasicType bt = (BasicType) bType;
            return at.equals(bt) ? BooleanTri.TRUE : BooleanTri.FALSE;
        }
        System.out.println(aType.getClass() + "\t" + bType.getClass() + "\t" + aType + "\t" + bType);
        return BooleanTri.MAYBE;
    }


    public static BooleanTri MethodSignaturesEqual(Method self, Method other) {
        if (self == other)
            return BooleanTri.TRUE;
        if (self.Backing.getAccessFlags() != other.Backing.getAccessFlags())
            return BooleanTri.FALSE;
        Fingerprint srcSelf = self.Parent.Parent;
        Fingerprint srcOther = other.Parent.Parent;

        Type[] myTypes = self.Backing.getArgumentTypes();
        Type[] otherTypes = other.Backing.getArgumentTypes();
        if (myTypes.length != otherTypes.length)
            return BooleanTri.FALSE;

        boolean allExplicit = true;
        for (int i = 0; i < Math.min(myTypes.length, otherTypes.length); i++) {
            BooleanTri res = ExplicitEquality.TypesEqual(srcSelf, myTypes[i], srcOther, otherTypes[i]);
            allExplicit &= res == BooleanTri.TRUE;
            if (res == BooleanTri.FALSE)
                return BooleanTri.FALSE;
        }

        {
            BooleanTri res = ExplicitEquality.TypesEqual(srcSelf, self.Backing.getReturnType(), srcOther, other.Backing.getReturnType());
            allExplicit &= res == BooleanTri.TRUE;
            if (res == BooleanTri.FALSE)
                return BooleanTri.FALSE;
        }
        return allExplicit ? BooleanTri.TRUE : BooleanTri.MAYBE;
    }
}

package equinox.util;

import java.util.*;

public class MultiMap {
    public static <TKey, TVal> void AddMultiList(Map<TKey, List<TVal>> map, TKey key, TVal val) {
        List<TVal> e = map.get(key);
        if (e == null)
            map.put(key, e = new ArrayList<>());
        e.add(val);
    }

    public static <TKey, TVal> void AddMultiSet(Map<TKey, Set<TVal>> map, TKey key, TVal val) {
        Set<TVal> e = map.get(key);
        if (e == null)
            map.put(key, e = new HashSet<>());
        e.add(val);
    }
}

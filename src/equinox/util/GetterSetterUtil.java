package equinox.util;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Method;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.generic.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetterSetterUtil {
    public final Method Accessor;
    public final Field AccessedField;
    public final boolean Writing;

    public GetterSetterUtil(Method accessor, Field accessedField, boolean writing) {
        Accessor = accessor;
        AccessedField = accessedField;
        Writing = writing;
    }

    public static GetterSetterUtil GetMethodInfo(Method m) {
        Code code = m.Backing.getCode();
        if (code == null)
            return null;
        if (code.getCode().length > 100)
            return null;
        InstructionList il = new InstructionList(code.getCode());
        ConstantPoolGen cpg = new ConstantPoolGen(code.getConstantPool());
        Instruction[] set = il.getInstructions();
        if (set.length > 10)
            return null;
        set = Arrays.stream(set).filter(x -> !(x instanceof ConversionInstruction)).toArray(x -> new Instruction[x]);
        List<GetterSetterUtil> options = new ArrayList<>(2);
        for (int id = 0; id < set.length; id++) {
            Instruction i = set[id];
            if (i instanceof FieldInstruction) {
                FieldInstruction fi = (FieldInstruction) i;
                Type containerType = fi.getReferenceType(cpg);
                if (!(containerType instanceof ObjectType) || !((ObjectType) containerType).getClassName().equals(m.Parent.Backing.getClassName()))
                    continue;
                Class classContainer = m.Parent.Parent.GetClass(fi.getClassName(cpg));
                Field field = classContainer.GetField(fi.getFieldName(cpg));
                if (field == null)
                    continue;
                Instruction valueInstruction = null;
                int offset = 1;
                if (i instanceof PUTSTATIC || i instanceof PUTFIELD) {
                    if (id - offset < 0)
                        continue;
                    valueInstruction = set[id - offset];
                    offset++;
                }
                Instruction instanceInstruction = null;
                if (!field.Backing.isStatic()) {
                    if (m.Parent != classContainer)
                        continue;
                    if (id - offset < 0)
                        continue;
                    instanceInstruction = set[id - offset];
                    offset++;
                }
                if (instanceInstruction != null && (!(instanceInstruction instanceof ALOAD) || ((ALOAD) instanceInstruction).getIndex() != 0))
                    continue;
                if (valueInstruction != null && (!(valueInstruction instanceof LoadInstruction) || ((LoadInstruction) valueInstruction).getIndex() != (m.Backing.isStatic() ? 0 : 1)))
                    continue;

                options.add(new GetterSetterUtil(m, field, i instanceof PUTSTATIC || i instanceof PUTFIELD));
            }
        }
        return options.size() == 1 ? options.get(0) : null;
    }

    @Override
    public String toString() {
        return Accessor + " " + (Writing ? "writes" : "reads") + " " + AccessedField;
    }
}

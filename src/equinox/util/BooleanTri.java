package equinox.util;

public enum BooleanTri {
    FALSE, MAYBE, TRUE
}

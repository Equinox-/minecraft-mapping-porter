package equinox.util;

import java.util.ArrayList;
import java.util.List;

public class GenericAwareType {
    public final GenericAwareType NestedFor;
    public final String GenericType;
    public final GenericAwareType[] GenericArgs;

    public GenericAwareType(GenericAwareType nestedFor, String type, GenericAwareType[] args) {
        NestedFor = nestedFor;
        GenericType = type;
        GenericArgs = args;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GenericAwareType))
            return false;
        GenericAwareType other = (GenericAwareType) obj;
        if (!other.GenericType.equals(GenericType))
            return false;
        if (NestedFor == null && other.NestedFor != null)
            return false;
        if (NestedFor != null && !NestedFor.equals(other.NestedFor))
            return false;
        if (GenericArgs == null || GenericArgs.length == 0)
            return other.GenericArgs == null || other.GenericArgs.length == 0;
        if (other.GenericArgs == null || other.GenericArgs.length == 0)
            return false;
        if (other.GenericArgs.length != GenericArgs.length)
            return false;
        for (int i = 0; i < GenericArgs.length; i++)
            if (GenericArgs[i] != null ? !GenericArgs[i].equals(other.GenericArgs[i]) : (other.GenericArgs[i] == null))
                return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        if (NestedFor != null)
            hash = (hash * 31) ^ NestedFor.hashCode();
        hash = (hash * 31) ^ GenericType.hashCode();
        if (GenericArgs != null)
            for (GenericAwareType t : GenericArgs)
                hash = (hash * 31) ^ (t != null ? t.hashCode() : 1234);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (NestedFor != null)
            sb.append(NestedFor.toString()).append('.');
        sb.append(GenericType);
        if (GenericArgs != null && GenericArgs.length > 0) {
            sb.append('<');
            boolean first = true;
            for (GenericAwareType gat : GenericArgs) {
                if (!first)
                    sb.append(", ");
                sb.append(gat == null ? "*" : gat.toString());
                first = false;
            }
            sb.append('>');
        }
        return sb.toString();
    }

    private static int DecodePart(String sig, int left, List<Object> data) {
        char first = sig.charAt(left);
        if (first == '+' || first == '-')
            left++;
        else if (first == '*')
            return left + 1;
        int rank = 0;
        while (sig.charAt(left + rank) == '[')
            rank++;
        int typeStart = left + rank;
        char sigTypeCode = sig.charAt(typeStart);
        boolean isType = sigTypeCode == 'L' || sigTypeCode == 'T';
        if (!isType) {
            data.add(sig.substring(left, typeStart + 1));
            return typeStart + 1;
        }
        int endOfType = sig.indexOf('<', typeStart);
        int endOfType2 = sig.indexOf(';', typeStart);
        if (endOfType == -1 || endOfType2 < endOfType) {
            assert endOfType2 > left : "Signature " + sig.substring(left) + " doesn't contain a type";
            data.add(sig.substring(left, endOfType2) + ";");
            return endOfType2 + 1;
        }
        data.add(sig.substring(left, endOfType) + ";");
        int i = endOfType + 1;
        while (true) {
            char c = sig.charAt(i);
            if (c == '>') {
                char next = sig.charAt(i + 1);
                if (next == '.') {
                    // TODO hacked
                    data.clear();
                    while ((next = sig.charAt(i + 1)) != ';')
                        i++;
                }
                assert next == ';' : "Dangling modifiers in " + sig.substring(left);
                i++;
                break;
            }
            if (c == ';') {
                break;
            }
            List<Object> tmp = new ArrayList<>();
            i = DecodePart(sig, i, tmp);
            data.add(tmp);
        }
        return i + 1;
    }

    private static List<Object> DecodeInternal(String signature) {
        List<Object> dat = new ArrayList<>();
        if (DecodePart(signature, 0, dat) != signature.length())
            throw new RuntimeException("Bad decode of " + signature);
        return dat;
    }

    private static GenericAwareType DecodeInternal(List<Object> data) {
        if (data.size() == 0)
            return null;
        int offset = 0;
        GenericAwareType nested = null;
        if (data.get(0) instanceof List) {
            offset = 1;
            nested = DecodeInternal((List<Object>) data.get(0));
        }
        GenericAwareType[] args = null;
        if (data.size() > 1) {
            args = new GenericAwareType[data.size() - 1];
            for (int i = 0; i < args.length; i++)
                args[i] = DecodeInternal((List<Object>) data.get(i + offset + 1));
        }
        return new GenericAwareType(nested, (String) data.get(offset), args);
    }

    public static GenericAwareType Decode(String signature) {
        List<Object> k = DecodeInternal(signature);
        return DecodeInternal(k);
    }
}

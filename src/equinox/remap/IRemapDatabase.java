package equinox.remap;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Method;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;

public interface IRemapDatabase {
    String RemappedClassName(String clazz);
    String ReversedClassName(String clazz);

    boolean IsClassRemapped(String clazz);

    default String RemappedName(Class clazz) {
        return RemappedClassName(clazz.Backing.getClassName());
    }

    String RemappedName(Method method);
    String RemappedName(Field field);

    default boolean IsRemapped(Class clazz) {
        return IsClassRemapped(clazz.Backing.getClassName());
    }

    boolean IsRemapped(Method method);
    boolean IsRemapped(Field field);

    default Type RemapType(Type ret) {
        if (ret instanceof ArrayType) {
            Type em = RemapType(((ArrayType) ret).getElementType());
            return new ArrayType(em, ((ArrayType) ret).getDimensions());
        } else if (ret instanceof ObjectType) {
            ObjectType ot = (ObjectType) ret;
            String remapped = RemappedClassName(ot.getClassName());
            if (remapped != null)
                return new ObjectType(remapped);
        }
        return ret;
    }
}

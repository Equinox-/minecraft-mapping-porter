package equinox.remap;

public interface IRemappedElement {
    String getObfuscatedName();
    String getRemappedName();

    default boolean isRemapped() {
        return !getObfuscatedName().equals(getRemappedName());
    }
}

package equinox.remap;

import com.sun.javaws.exceptions.InvalidArgumentException;
import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Method;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class CustomRemap implements IEnumerableRemapDatabase {
    private final Map<String, String> _classNameRemap = new HashMap<>();
    private final Map<String, String> _classNameRemapReversed = new HashMap<>();
    private final Map<Class, String> _classRemap = new HashMap<>();
    private final Map<Field, String> _fieldRemap = new HashMap<>();
    private final Map<Method, String> _methodRemap = new HashMap<>();

    @Override
    public Stream<Class> RemappedClasses() {
        return _classRemap.keySet().stream();
    }

    @Override
    public Stream<Method> RemappedMethods() {
        return _methodRemap.keySet().stream();
    }

    @Override
    public Stream<Field> RemappedFields() {
        return _fieldRemap.keySet().stream();
    }

    @Override
    public String RemappedClassName(String clazz) {
        return _classNameRemap.getOrDefault(clazz, clazz);
    }

    @Override
    public String ReversedClassName(String clazz) {
        return _classNameRemapReversed.getOrDefault(clazz, clazz);
    }

    @Override
    public boolean IsClassRemapped(String clazz) {
        return _classNameRemap.containsKey(clazz);
    }

    @Override
    public String RemappedName(Method method) {
        return _methodRemap.getOrDefault(method, method.Backing.getName());
    }

    @Override
    public String RemappedName(Field field) {
        return _fieldRemap.getOrDefault(field, field.Backing.getName());
    }

    @Override
    public boolean IsRemapped(Method method) {
        return _methodRemap.containsKey(method);
    }

    @Override
    public boolean IsRemapped(Field field) {
        return _fieldRemap.containsKey(field);
    }

    public void Remap(Class clazz, String name) {
        if (name == null) {
            String old = _classRemap.get(clazz);
            if (old != null)
                _classNameRemapReversed.remove(old);
            _classRemap.remove(clazz);
            _classNameRemap.remove(clazz.Backing.getClassName());
            return;
        }
        if (name.trim().length() == 0)
            throw new IllegalArgumentException("Must be null or a non-whitespace string");
        if (clazz.Backing.getClassName().equals(name))
            throw new RuntimeException("Trying to remap " + clazz + " to identical name.  This shouldn't occur");
        _classRemap.put(clazz, name);
        _classNameRemapReversed.put(name, clazz.Backing.getClassName());
        _classNameRemap.put(clazz.Backing.getClassName(), name);
    }

    public void Remap(Method method, String name) {
        if (name == null) {
            _methodRemap.remove(method);
            return;
        }
        if (method.Backing.getName().equals(name))
            throw new RuntimeException("Trying to remap " + method + " to identical name.  This shouldn't occur");
        _methodRemap.put(method, name);
    }

    public void Remap(Field field, String name) {
        if (name == null) {
            _fieldRemap.remove(field);
            return;
        }
        if (field.Backing.getName().equals(name))
            throw new RuntimeException("Trying to remap " + field + " to identical name.  This shouldn't occur");
        _fieldRemap.put(field, name);
    }
}

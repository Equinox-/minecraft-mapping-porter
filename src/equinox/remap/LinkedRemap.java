package equinox.remap;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Method;

import java.util.*;
import java.util.stream.Stream;

public class LinkedRemap implements IEnumerableRemapDatabase {
    private final Map<String, Class> _classLinkName = new HashMap<>();
    private final Map<Class, Class> _classLink = new HashMap<>();
    private final Map<Field, Field> _fieldLink = new HashMap<>();
    private final Map<Method, Method> _methodLink = new HashMap<>();
    private final Map<Class, Class> _classLinkRev = new HashMap<>();
    private final Map<String, Class> _classLinkNameRev = new HashMap<>();
    private final Map<Field, Field> _fieldLinkRev = new HashMap<>();
    private final Map<Method, Method> _methodLinkRev = new HashMap<>();
    private final IRemapDatabase _backing;

    private final Set<Object> _blacklist = new HashSet<Object>();

    public LinkedRemap(IRemapDatabase backing) {
        _backing = backing;
    }

    public boolean Link(Class from, Class to) {
        if (_blacklist.contains(to) || _blacklist.contains(from))
            return false;
        Class oldTo = _classLink.get(from);
        if (oldTo == to)
            return false;
        if (oldTo != null)
            _classLinkRev.remove(oldTo);
        if (_classLinkRev.containsKey(to)) {
            Class oldFrom = _classLinkRev.remove(to);
            _blacklist.add(to);
            _blacklist.add(from);
            _blacklist.add(oldFrom);

            _classLink.remove(oldFrom);
            _classLinkName.remove(oldFrom.Backing.getClassName());
            _classLinkNameRev.remove(to.Backing.getClassName());
            return false;
        }

        _classLinkName.put(from.Backing.getClassName(), to);
        _classLink.put(from, to);
        _classLinkRev.put(to, from);
        _classLinkNameRev.put(to.Backing.getClassName(), from);
        return true;
    }

    public boolean Link(Field from, Field to) {
        if (_blacklist.contains(to) || _blacklist.contains(from))
            return false;
        Field oldTo = _fieldLink.get(from);
        if (oldTo == to)
            return false;
        if (oldTo != null)
            _fieldLinkRev.remove(oldTo);
        if (_fieldLinkRev.containsKey(to)) {
            Field oldFrom = _fieldLinkRev.remove(to);
            System.err.println(to + " is already mapped to " + oldFrom);
            _blacklist.add(to);
            _blacklist.add(from);
            _blacklist.add(oldFrom);
            _fieldLink.remove(oldFrom);
            return false;
        }
        _fieldLink.put(from, to);
        _fieldLinkRev.put(to, from);
        return true;
    }

    public boolean Link(Method from, Method to) {
        if (_blacklist.contains(to) || _blacklist.contains(from))
            return false;
        Method oldTo = _methodLink.get(from);
        if (oldTo == to)
            return false;
        if (oldTo != null)
            _methodLinkRev.remove(oldTo);
        if (_methodLinkRev.containsKey(to)) {
            Method oldFrom = _methodLinkRev.remove(to);
            System.err.println(to + " is already mapped to " + oldFrom);
            _blacklist.add(to);
            _blacklist.add(from);
            _blacklist.add(oldFrom);
            _methodLink.remove(oldFrom);
            return false;
        }
        _methodLink.put(from, to);
        _methodLinkRev.put(to, from);
        return true;
    }

    public Stream<Map.Entry<Class, Class>> LinkedClasses() {
        return _classLink.entrySet().stream();
    }

    public Stream<Map.Entry<Field, Field>> LinkedFields() {
        return _fieldLink.entrySet().stream();
    }

    public Stream<Map.Entry<Method, Method>> LinkedMethods() {
        return _methodLink.entrySet().stream();
    }

    public Class Linked(Class from) {
        return _classLink.getOrDefault(from, null);
    }

    public Field Linked(Field from) {
        return _fieldLink.getOrDefault(from, null);
    }

    public Method Linked(Method from) {
        return _methodLink.getOrDefault(from, null);
    }

    public Class LinkedReverse(Class to) {
        return _classLinkRev.getOrDefault(to, null);
    }

    public Field LinkedReverse(Field to) {
        return _fieldLinkRev.getOrDefault(to, null);
    }

    public Method LinkedReverse(Method to) {
        return _methodLinkRev.getOrDefault(to, null);
    }

    @Override
    public String RemappedClassName(String clazz) {
        Class opposite = _classLinkName.getOrDefault(clazz, null);
        if (opposite != null)
            return _backing.RemappedName(opposite);
        return clazz;
    }

    @Override
    public String ReversedClassName(String clazz) {
        String opposite = _backing.ReversedClassName(clazz);
        if (opposite != null) {
            Class data = _classLinkNameRev.getOrDefault(opposite, null);
            if (data != null)
                return data.Backing.getClassName();
        }
        return clazz;
    }

    @Override
    public boolean IsClassRemapped(String clazz) {
        Class opposite = _classLinkName.getOrDefault(clazz, null);
        if (opposite != null)
            return _backing.IsRemapped(opposite);
        return false;
    }

    @Override
    public String RemappedName(Method method) {
        Method opposite = _methodLink.getOrDefault(method, null);
        if (opposite != null)
            return _backing.RemappedName(opposite);
        return method.Backing.getName();
    }

    @Override
    public boolean IsRemapped(Method method) {
        Method opposite = _methodLink.getOrDefault(method, null);
        if (opposite != null)
            return _backing.IsRemapped(opposite);
        return false;
    }

    @Override
    public String RemappedName(Field field) {
        Field opposite = _fieldLink.getOrDefault(field, null);
        if (opposite != null)
            return _backing.RemappedName(opposite);
        return field.Backing.getName();
    }

    @Override
    public boolean IsRemapped(Field field) {
        Field opposite = _fieldLink.getOrDefault(field, null);
        if (opposite != null)
            return _backing.IsRemapped(opposite);
        return false;
    }

    @Override
    public Stream<Class> RemappedClasses() {
        return _classLink.keySet().stream().filter(this::IsRemapped);
    }

    @Override
    public Stream<Method> RemappedMethods() {
        return _methodLink.keySet().stream().filter(this::IsRemapped);
    }

    @Override
    public Stream<Field> RemappedFields() {
        return _fieldLink.keySet().stream().filter(this::IsRemapped);
    }
}

package equinox.remap;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Method;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;

import java.io.*;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FileBackedRemap implements IRemapDatabase {
    private final Map<String, String> _classRemap;
    private final Map<String, String> _reversedClassRemap;
    private final Map<MethodKey, String> _methodRemap;
    private final Map<FieldKey, String> _fieldRemap;

    public FileBackedRemap() {
        _classRemap = new HashMap<>();
        _methodRemap = new HashMap<>();
        _fieldRemap = new HashMap<>();
        _reversedClassRemap = new HashMap<>();
    }

    public static FileBackedRemap Read(File dir, String prefix) {
        File remapFile = new File(dir, prefix + ".tsrg");
        if (!remapFile.exists())
            remapFile = new File(dir, prefix + ".csrg");
        if (!remapFile.exists())
            remapFile = new File(dir, prefix + ".srg");
        if (remapFile.exists()) {
            FileBackedRemap fbr = new FileBackedRemap();
            fbr.Read(remapFile);
            return fbr;
        }
        throw new RuntimeException("Could not find " + remapFile.toString());
    }

    public static FileBackedRemap Create(IEnumerableRemapDatabase remap) {
        FileBackedRemap result = new FileBackedRemap();
        remap.RemappedClasses().filter(x -> !x.Backing.getClassName().equals(remap.RemappedName(x)))
                .forEach(x -> result.RemapClass(x.Backing.getClassName(), remap.RemappedName(x)));
        remap.RemappedFields().filter(x -> !x.Backing.getName().equals(remap.RemappedName(x)))
                .forEach(x -> result._fieldRemap.put(new FieldKey(x.Parent.Backing.getClassName(), x.Backing.getName()), remap.RemappedName(x)));
        remap.RemappedMethods().filter(x -> !x.Backing.getName().equals(remap.RemappedName(x)))
                .forEach(x -> result._methodRemap.put(new MethodKey(x.Parent.Backing.getClassName(), x.Backing.getName(), x.Backing.getSignature()), remap.RemappedName(x)));
        return result;
    }

    private void RemapClass(String old, String nw) {
        _classRemap.put(old, nw);
        if (nw == null || nw.trim().length() == 0)
            throw new IllegalArgumentException("New class name must not be null or empty");
        _reversedClassRemap.put(nw, old);
    }


    private String purifyName(String name) {
        int idx = name.lastIndexOf('/');
        if (idx == -1)
            return name;
        return name.substring(idx + 1);
    }

    public void Read(File f) {
        boolean csrg = f.getAbsolutePath().toLowerCase().endsWith(".csrg");
        boolean tsrg = f.getAbsolutePath().toLowerCase().endsWith(".tsrg");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(f));
            String lastClassEntry = null;
            while (true) {
                String line = reader.readLine();
                if (line == null)
                    break;
                if (tsrg && line.startsWith("\t")) {
                    if (lastClassEntry == null)
                        throw new IOException("No active class");
                    line = lastClassEntry + " " + line.substring(1);
                }
                String[] parts = line.split(" ");
                if (csrg || tsrg) {
                    if (parts.length == 2) {
                        lastClassEntry = parts[0];
                        RemapClass(parts[0], parts[1]);
                    } else if (parts.length == 3) {
                        _fieldRemap.put(new FieldKey(parts[0], parts[1]), purifyName(parts[2]));
                    } else if (parts.length == 4) {
                        // TODO technically wrong signature in target type
                        _methodRemap.put(new MethodKey(parts[0], parts[1], parts[2]), purifyName(parts[3]));
                    }
                } else {
                    if (parts[0].equals("CL:"))
                        RemapClass(parts[1], parts[2]);
                    else if (parts[0].equals("FD:"))
                        _fieldRemap.put(new FieldKey(parts[1]), purifyName(parts[2]));
                    else if (parts[0].equals("MD:"))
                        _methodRemap.put(new MethodKey(parts[1], parts[2]), purifyName(parts[3]));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void Save(File f) {
        Save(f, null);
    }

    public void Save(File f, String forcedExt) {
        boolean csrg = (forcedExt != null && forcedExt.equalsIgnoreCase("csrg")) || f.getAbsolutePath().toLowerCase().endsWith(".csrg");
        boolean tsrg = (forcedExt != null && forcedExt.equalsIgnoreCase("tsrg")) || f.getAbsolutePath().toLowerCase().endsWith(".tsrg");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(f));
            if (csrg || tsrg) {
                List<Map.Entry<FieldKey, String>> orderedFields = _fieldRemap.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).collect(Collectors.toList());
                List<Map.Entry<MethodKey, String>> orderedMethods = _methodRemap.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).collect(Collectors.toList());
                for (Map.Entry<String, String> clazzRemap : _classRemap.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).collect(Collectors.toList())) {
                    writer.write(clazzRemap.getKey());
                    writer.write(" ");
                    writer.write(clazzRemap.getValue());
                    writer.newLine();
                    for (Map.Entry<FieldKey, String> fr : orderedFields) {
                        if (!fr.getKey().ClassName.equals(clazzRemap.getKey()))
                            continue;
                        if (tsrg)
                            writer.write("\t");
                        else {
                            writer.write(fr.getKey().ClassName);
                            writer.write(" ");
                        }
                        writer.write(fr.getKey().FieldName);
                        writer.write(" ");
                        writer.write(fr.getValue());
                        writer.newLine();
                    }
                    for (Map.Entry<MethodKey, String> mr : orderedMethods) {
                        if (!mr.getKey().ClassName.equals(clazzRemap.getKey()))
                            continue;
                        if (tsrg)
                            writer.write("\t");
                        else {
                            writer.write(mr.getKey().ClassName);
                            writer.write(" ");
                        }
                        writer.write(mr.getKey().MethodName);
                        writer.write(" ");
                        writer.write(mr.getKey().Signature);
                        writer.write(" ");
                        writer.write(mr.getValue());
                        writer.newLine();
                    }
                }
            } else {
                for (Map.Entry<String, String> clazzRemap : _classRemap.entrySet()) {
                    writer.write("CL: ");
                    writer.write(clazzRemap.getKey());
                    writer.write(" ");
                    writer.write(clazzRemap.getValue());
                    writer.newLine();
                }
                for (Map.Entry<MethodKey, String> mr : _methodRemap.entrySet()) {
                    writer.write("MD: ");
                    writer.write(mr.getKey().ClassName);
                    writer.write("/");
                    writer.write(mr.getKey().MethodName);
                    writer.write(" ");
                    writer.write(mr.getKey().Signature);
                    writer.write(" ");
                    writer.write(_classRemap.getOrDefault(mr.getKey().ClassName, mr.getKey().ClassName));
                    writer.write("/");
                    writer.write(mr.getValue());
                    writer.write(" ");
                    writer.write(mr.getKey().Signature);
                    writer.newLine();
                }
                for (Map.Entry<FieldKey, String> fr : _fieldRemap.entrySet()) {
                    writer.write("FD: ");
                    writer.write(fr.getKey().ClassName);
                    writer.write("/");
                    writer.write(fr.getKey().FieldName);
                    writer.write(" ");
                    writer.write(_classRemap.getOrDefault(fr.getKey().ClassName, fr.getKey().ClassName));
                    writer.write("/");
                    writer.write(fr.getValue());
                    writer.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String RemappedClassName(String clazz) {
        return _classRemap.getOrDefault(clazz, clazz);
    }

    @Override
    public String ReversedClassName(String clazz) {
        return _reversedClassRemap.getOrDefault(clazz, clazz);
    }

    @Override
    public boolean IsClassRemapped(String clazz) {
        return _classRemap.containsKey(clazz);
    }

    @Override
    public String RemappedName(Method method) {
        return _methodRemap.getOrDefault(new MethodKey(method.Parent.Backing.getClassName(), method.Backing.getName(), method.Backing.getSignature()), method.Backing.getName());
    }

    @Override
    public String RemappedName(Field field) {
        return _fieldRemap.getOrDefault(new FieldKey(field.Parent.Backing.getClassName(), field.Backing.getName()), field.Backing.getName());
    }

    @Override
    public boolean IsRemapped(Method method) {
        return _methodRemap.containsKey(new MethodKey(method.Parent.Backing.getClassName(), method.Backing.getName(), method.Backing.getSignature()));
    }

    @Override
    public boolean IsRemapped(Field field) {
        return _fieldRemap.containsKey(new FieldKey(field.Parent.Backing.getClassName(), field.Backing.getName()));
    }

    private static class FieldKey {
        public final String ClassName;
        public final String FieldName;

        public FieldKey(String res) {
            int s = res.lastIndexOf('/');
            ClassName = res.substring(0, s);
            FieldName = res.substring(s + 1);
        }

        public FieldKey(String className, String fieldName) {
            ClassName = className;
            FieldName = fieldName;
        }

        @Override
        public int hashCode() {
            return (ClassName.hashCode() * 31) ^ (FieldName.hashCode() * 7);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof FieldKey))
                return false;

            FieldKey mr = (FieldKey) obj;

            return ClassName.equals(mr.ClassName) && FieldName.equals(mr.FieldName);
        }

        @Override
        public String toString() {
            return ClassName + "#" + FieldName;
        }
    }

    private static class MethodKey {
        public final String ClassName;
        public final String MethodName;
        public final String Signature;

        public MethodKey(String res, String sig) {
            int s = res.lastIndexOf('/');
            ClassName = res.substring(0, s);
            MethodName = res.substring(s + 1);
            Signature = sig;
        }

        public MethodKey(String className, String methodName, String signature) {
            ClassName = className;
            MethodName = methodName;
            Signature = signature;
        }

        @Override
        public int hashCode() {
            return (ClassName.hashCode() * 31) ^ (MethodName.hashCode() * 7) ^ Signature.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof MethodKey))
                return false;

            MethodKey mr = (MethodKey) obj;

            return ClassName.equals(mr.ClassName) && MethodName.equals(mr.MethodName) && Signature.equals(mr.Signature);
        }

        @Override
        public String toString() {
            return ClassName + "#" + MethodName + "//" + Signature;
        }
    }
}

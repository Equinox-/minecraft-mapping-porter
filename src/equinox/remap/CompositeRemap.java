package equinox.remap;

import equinox.fingerprint.Class;
import equinox.fingerprint.Field;
import equinox.fingerprint.Method;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class CompositeRemap implements IEnumerableRemapDatabase {
    public final IEnumerableRemapDatabase[] Databases;

    public CompositeRemap(IEnumerableRemapDatabase... databases) {
        Databases = databases;
    }

    @Override
    public Stream<Class> RemappedClasses() {
        Set<Class> mapped = new HashSet<>();
        for (IEnumerableRemapDatabase e : Databases)
            e.RemappedClasses().forEach(x -> mapped.add(x));
        return mapped.stream().filter(this::IsRemapped);
    }

    @Override
    public Stream<Method> RemappedMethods() {
        Set<Method> mapped = new HashSet<>();
        for (IEnumerableRemapDatabase e : Databases)
            e.RemappedMethods().forEach(x -> mapped.add(x));
        return mapped.stream().filter(this::IsRemapped);
    }

    @Override
    public Stream<Field> RemappedFields() {
        Set<Field> mapped = new HashSet<>();
        for (IEnumerableRemapDatabase e : Databases)
            e.RemappedFields().forEach(x -> mapped.add(x));
        return mapped.stream().filter(this::IsRemapped);
    }

    @Override
    public String RemappedClassName(String clazz) {
        for (IEnumerableRemapDatabase e : Databases)
            if (e.IsClassRemapped(clazz))
                return e.RemappedClassName(clazz);
        return clazz;
    }

    @Override
    public String ReversedClassName(String clazz) {
        for (IEnumerableRemapDatabase e : Databases) {
            String reversed = e.ReversedClassName(clazz);
            if (e.IsClassRemapped(reversed))
                return reversed;
        }
        return clazz;
    }

    @Override
    public boolean IsClassRemapped(String clazz) {
        for (IEnumerableRemapDatabase e : Databases)
            if (e.IsClassRemapped(clazz))
                return true;
        return false;
    }

    @Override
    public String RemappedName(Method method) {
        for (IEnumerableRemapDatabase e : Databases)
            if (e.IsRemapped(method))
                return e.RemappedName(method);
        return method.Backing.getName();
    }

    @Override
    public String RemappedName(Field field) {
        for (IEnumerableRemapDatabase e : Databases)
            if (e.IsRemapped(field))
                return e.RemappedName(field);
        return field.Backing.getName();
    }

    @Override
    public boolean IsRemapped(Method method) {
        for (IEnumerableRemapDatabase e : Databases)
            if (e.IsRemapped(method))
                return true;
        return false;
    }

    @Override
    public boolean IsRemapped(Field field) {
        for (IEnumerableRemapDatabase e : Databases)
            if (e.IsRemapped(field))
                return true;
        return false;
    }
}

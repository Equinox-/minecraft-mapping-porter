package equinox.remap;

import equinox.fingerprint.Field;
import equinox.fingerprint.Method;
import equinox.fingerprint.Class;

import java.util.stream.Stream;

public interface IEnumerableRemapDatabase extends IRemapDatabase {
    Stream<Class> RemappedClasses();
    Stream<Method> RemappedMethods();
    Stream<Field> RemappedFields();
}
